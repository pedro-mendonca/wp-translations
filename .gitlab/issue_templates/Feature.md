## Prerequisites

Hello! We love receiving contributions from our community, so thanks for your new idea to improve our plugin.


Please answer the following questions for yourself before submitting an issue.

- [ ] I am running the latest version
- [ ] I checked the documentation and found no answer (you should doc is underconstruction)
- [ ] I checked to make sure that this feature has not already been asked

## Context

Please provide any relevant information about your setup. This is important in case the feature is not reproducible except for under certain conditions.

* WordPress Version:
* Multisite:
* Operating System:
* Plugin version:


## Expected Behavior

Please describe the behavior you are expecting

## Current Behavior

What is the current behavior if any?


### Steps to Reproduce

Please provide detailed steps to get to your point.

1. step 1
2. step 2
3. you get it...


/label ~feature
