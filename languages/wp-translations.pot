# Copyright (C) 2018 WP_Translations
# This file is distributed under the same license as the WP_Translations package.
msgid ""
msgstr ""
"Project-Id-Version: WP_Translations\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language-Team: WP-Translations <contact@wp-translations.pro>\n"
"Last-Translator: FX Bénard <fx@wp-translations.pro>\n"
"Report-Msgid-Bugs-To: https://wp-translations.org\n"
"X-Poedit-Basepath: ..\n"
"X-Poedit-KeywordsList: __;_e;_ex:1,2c;_n:1,2;_n_noop:1,2;_nx:1,2,4c;_nx_noop:1,2,3c;_x:1,2c;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c\n"
"X-Poedit-SearchPath-0: .\n"
"X-Poedit-SearchPathExcluded-0: *.js\n"
"X-Poedit-SourceCharset: UTF-8\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: src/WP_Translations/WordPress/Admin/EnqueueAssets.php:78
msgid "No data available"
msgstr ""

#: src/WP_Translations/WordPress/Admin/EnqueueAssets.php:79
msgctxt "Please don't translate: _START_ _END_ _TOTAL_"
msgid "Showing _START_ to _END_ of _TOTAL_ entries"
msgstr ""

#: src/WP_Translations/WordPress/Admin/EnqueueAssets.php:80
msgid "Showing 0 to 0 of 0 entries"
msgstr ""

#: src/WP_Translations/WordPress/Admin/EnqueueAssets.php:81
msgctxt "Please don't translate: _MAX_"
msgid "filtered from _MAX_ total entries"
msgstr ""

#: src/WP_Translations/WordPress/Admin/EnqueueAssets.php:82
msgctxt "Please don't translate: _MENU_"
msgid "Show _MENU_ entries"
msgstr ""

#: src/WP_Translations/WordPress/Admin/EnqueueAssets.php:83
msgid "Processing..."
msgstr ""

#: src/WP_Translations/WordPress/Admin/EnqueueAssets.php:84
msgid "Search:"
msgstr ""

#: src/WP_Translations/WordPress/Admin/EnqueueAssets.php:85
msgid "No matching records found"
msgstr ""

#: src/WP_Translations/WordPress/Admin/EnqueueAssets.php:86
msgid "First"
msgstr ""

#: src/WP_Translations/WordPress/Admin/EnqueueAssets.php:87
msgid "Last"
msgstr ""

#: src/WP_Translations/WordPress/Admin/EnqueueAssets.php:88
msgid "Next"
msgstr ""

#: src/WP_Translations/WordPress/Admin/EnqueueAssets.php:89
msgid "Previous"
msgstr ""

#: src/WP_Translations/WordPress/Admin/EnqueueAssets.php:90
msgid ": activate to sort column ascending"
msgstr ""

#: src/WP_Translations/WordPress/Admin/EnqueueAssets.php:91
msgid ": activate to sort column descending"
msgstr ""

#: src/WP_Translations/WordPress/Admin/EnqueueAssets.php:123
msgid "Save License"
msgstr ""

#: src/WP_Translations/WordPress/Admin/EnqueueAssets.php:124
msgid "Delete License"
msgstr ""

#: src/WP_Translations/WordPress/Admin/EnqueueAssets.php:125, src/WP_Translations/WordPress/Helpers/RepositoryHelper.php:387, src/WP_Translations/WordPress/Admin/Page/PagePremium.php:220, src/WP_Translations/WordPress/Admin/Page/PagePremium.php:220, src/WP_Translations/WordPress/Admin/Page/PagePremium.php:228, src/WP_Translations/WordPress/Admin/Page/PagePremium.php:228, src/WP_Translations/WordPress/Admin/Page/PagePremium.php:234, src/WP_Translations/WordPress/Admin/Page/PagePremium.php:234, src/WP_Translations/WordPress/Admin/Page/PageRepositories.php:157
msgid "Activate"
msgstr ""

#: src/WP_Translations/WordPress/Admin/EnqueueAssets.php:126, src/WP_Translations/WordPress/Admin/Page/PagePremium.php:208, src/WP_Translations/WordPress/Admin/Page/PagePremium.php:208, src/WP_Translations/WordPress/Admin/Page/PageRepositories.php:159
msgid "Deactivate"
msgstr ""

#: src/WP_Translations/WordPress/Admin/EnqueueAssets.php:127, src/WP_Translations/WordPress/Admin/Page/PagePremium.php:227, src/WP_Translations/WordPress/Admin/Page/PagePremium.php:227
msgid "Renew"
msgstr ""

#: src/WP_Translations/WordPress/Admin/EnqueueAssets.php:128, src/WP_Translations/WordPress/Admin/Page/PagePremium.php:217, src/WP_Translations/WordPress/Admin/Page/PagePremium.php:218
msgid "Upgrade"
msgstr ""

#: src/WP_Translations/WordPress/Admin/EnqueueAssets.php:129
msgid "Install translations"
msgstr ""

#: src/WP_Translations/WordPress/Admin/EnqueueAssets.php:130
msgid "Activated and installed"
msgstr ""

#: src/WP_Translations/WordPress/Admin/EnqueueAssets.php:131, src/WP_Translations/WordPress/Admin/Actions/CapabilityActions.php:28
msgid "Add a new rule"
msgstr ""

#: src/WP_Translations/WordPress/Admin/EnqueueAssets.php:134
msgid "License expired"
msgstr ""

#: src/WP_Translations/WordPress/Admin/EnqueueAssets.php:135
msgid "License activated"
msgstr ""

#: src/WP_Translations/WordPress/Admin/EnqueueAssets.php:136
msgid "License can be activated"
msgstr ""

#: src/WP_Translations/WordPress/Admin/EnqueueAssets.php:137
msgid "License deactivated"
msgstr ""

#: src/WP_Translations/WordPress/Admin/EnqueueAssets.php:140
msgid "Check license status"
msgstr ""

#: src/WP_Translations/WordPress/Admin/EnqueueAssets.php:141, src/WP_Translations/WordPress/Admin/EnqueueAssets.php:186
msgid "Downloading and installing your language pack"
msgstr ""

#: src/WP_Translations/WordPress/Admin/EnqueueAssets.php:142, src/WP_Translations/WordPress/Admin/EnqueueAssets.php:187
msgid "Check for updates"
msgstr ""

#: src/WP_Translations/WordPress/Admin/EnqueueAssets.php:143, src/WP_Translations/WordPress/Admin/EnqueueAssets.php:188
msgid "Clearing cache"
msgstr ""

#: src/WP_Translations/WordPress/Admin/EnqueueAssets.php:144, src/WP_Translations/WordPress/Admin/EnqueueAssets.php:189
msgid "Language pack installed!"
msgstr ""

#: src/WP_Translations/WordPress/Admin/EnqueueAssets.php:145
msgid "Deleting license"
msgstr ""

#: src/WP_Translations/WordPress/Admin/EnqueueAssets.php:146
msgid "Saving license"
msgstr ""

#: src/WP_Translations/WordPress/Admin/EnqueueAssets.php:147
msgid "Deactivating license"
msgstr ""

#: src/WP_Translations/WordPress/Admin/EnqueueAssets.php:150
msgid "Delete repository"
msgstr ""

#: src/WP_Translations/WordPress/Admin/EnqueueAssets.php:151
msgid "Deleting repository"
msgstr ""

#: src/WP_Translations/WordPress/Admin/EnqueueAssets.php:152
msgid "Saving repository"
msgstr ""

#: src/WP_Translations/WordPress/Admin/EnqueueAssets.php:153
msgid "Deactivating repository"
msgstr ""

#: src/WP_Translations/WordPress/Admin/EnqueueAssets.php:154
msgid "Activating repository"
msgstr ""

#: src/WP_Translations/WordPress/Admin/EnqueueAssets.php:155, src/WP_Translations/WordPress/Admin/Page/PageRepositories.php:166
msgid "Edit"
msgstr ""

#: src/WP_Translations/WordPress/Admin/EnqueueAssets.php:156, src/WP_Translations/WordPress/Admin/Page/PagePremium.php:136, src/WP_Translations/WordPress/Admin/Page/PagePremium.php:136
msgid "Close Modal"
msgstr ""

#: src/WP_Translations/WordPress/Admin/EnqueueAssets.php:157, src/WP_Translations/WordPress/Helpers/ModalHelper.php:254, src/WP_Translations/WordPress/Admin/Page/PageRepositories.php:21
msgid "Add a Repository"
msgstr ""

#: src/WP_Translations/WordPress/Admin/EnqueueAssets.php:161
msgid "Saving settings"
msgstr ""

#: src/WP_Translations/WordPress/Admin/EnqueueAssets.php:162, src/WP_Translations/WordPress/Helpers/RepositoryHelper.php:267, src/WP_Translations/WordPress/Admin/Page/PageRepositories.php:30, src/WP_Translations/WordPress/Admin/Page/PageTranslations.php:201
msgid "All"
msgstr ""

#: src/WP_Translations/WordPress/Admin/EnqueueAssets.php:163, src/WP_Translations/WordPress/Helpers/ModalHelper.php:199
msgid "Delete custom file"
msgstr ""

#: src/WP_Translations/WordPress/Admin/EnqueueAssets.php:164
msgid "Deleting custom file"
msgstr ""

#: src/WP_Translations/WordPress/Admin/EnqueueAssets.php:165
msgid "Custom file deleted"
msgstr ""

#: src/WP_Translations/WordPress/Admin/EnqueueAssets.php:168
msgid "Deleting translations files"
msgstr ""

#: src/WP_Translations/WordPress/Admin/EnqueueAssets.php:169
msgid "Installing translations files"
msgstr ""

#: src/WP_Translations/WordPress/Admin/EnqueueAssets.php:170
msgid "Translations files deleted"
msgstr ""

#: src/WP_Translations/WordPress/Admin/EnqueueAssets.php:171
msgid "Translations files installed"
msgstr ""

#: src/WP_Translations/WordPress/Admin/EnqueueAssets.php:172, src/WP_Translations/WordPress/Admin/Actions/LanguageActions.php:53, src/WP_Translations/WordPress/Admin/Actions/TranslationActions.php:122
msgid "Something went wrong."
msgstr ""

#: src/WP_Translations/WordPress/Admin/EnqueueAssets.php:181
msgid "Updating translations"
msgstr ""

#: src/WP_Translations/WordPress/Admin/EnqueueAssets.php:182
msgid "Translations updated"
msgstr ""

#: src/WP_Translations/WordPress/Admin/EnqueueAssets.php:183
msgid "The translations are up to date."
msgstr ""

#: src/WP_Translations/WordPress/Admin/EnqueueAssets.php:202, src/WP_Translations/WordPress/Admin/TranslationNotification.php:80
msgid "New translations are available:&nbsp;"
msgstr ""

#: src/WP_Translations/WordPress/Admin/EnqueueAssets.php:203, src/WP_Translations/WordPress/Admin/TranslationNotification.php:81, src/WP_Translations/WordPress/Admin/UpdateCore.php:133
msgid "Update now"
msgstr ""

#: src/WP_Translations/WordPress/Admin/EnqueueAssets.php:204, src/WP_Translations/WordPress/Admin/TranslationNotification.php:107
msgid "A premium translation is available!"
msgstr ""

#: src/WP_Translations/WordPress/Admin/EnqueueAssets.php:205, src/WP_Translations/WordPress/Helpers/ModalHelper.php:24, src/WP_Translations/WordPress/Helpers/ModalHelper.php:229
msgid "View details"
msgstr ""

#: src/WP_Translations/WordPress/Admin/Notice.php:33
msgid "Settings updated."
msgstr ""

#: src/WP_Translations/WordPress/Admin/Notice.php:37
msgid "Products list reloaded."
msgstr ""

#: src/WP_Translations/WordPress/Admin/Notice.php:41
msgid "Check for translations updates, done!"
msgstr ""

#: src/WP_Translations/WordPress/Admin/Notice.php:45
msgid "Translations settings successfully reseted."
msgstr ""

#: src/WP_Translations/WordPress/Admin/Notice.php:49
msgid "Logs successfully purged."
msgstr ""

#: src/WP_Translations/WordPress/Admin/Page.php:97, src/WP_Translations/WordPress/Admin/Page/PageLogs.php:21, src/WP_Translations/WordPress/Admin/Page/PageSettings.php:37
msgid "Updates"
msgstr ""

#: src/WP_Translations/WordPress/Admin/Page.php:103, src/WP_Translations/WordPress/Helpers/PageHelper.php:30, src/WP_Translations/WordPress/Admin/Page/PageLogs.php:183
msgid "Logs"
msgstr ""

#: src/WP_Translations/WordPress/Admin/Page.php:109, src/WP_Translations/WordPress/Admin/Actions/RepositoryActions.php:52, src/WP_Translations/WordPress/Admin/Page/PageRepositories.php:199, src/WP_Translations/WordPress/Admin/Page/PageSettings.php:81
msgid "Repositories"
msgstr ""

#: src/WP_Translations/WordPress/Admin/Page.php:131
msgid "Visit %1$s website | %2$sContact Support%3$s"
msgstr ""

#: src/WP_Translations/WordPress/Admin/Page.php:148
msgid "Help us with Translations"
msgstr ""

#: src/WP_Translations/WordPress/Admin/Page.php:149
msgid "Version:&nbsp;"
msgstr ""

#. translators: 1: Number of updates available to WordPress
#: src/WP_Translations/WordPress/Admin/TranslationSetup.php:137
msgid "%d WordPress Update"
msgstr ""

#. translators: 1: Number of updates available to plugins
#: src/WP_Translations/WordPress/Admin/TranslationSetup.php:141
msgid "%d Plugin Update"
msgid_plural "%d Plugin Updates"
msgstr[0] ""
msgstr[1] ""

#. translators: 1: Number of updates available to themes
#: src/WP_Translations/WordPress/Admin/TranslationSetup.php:145
msgid "%d Theme Update"
msgid_plural "%d Theme Updates"
msgstr[0] ""
msgstr[1] ""

#. translators: 1: Number of updates available to translations
#: src/WP_Translations/WordPress/Admin/TranslationSetup.php:149
msgid "%d Translation Update"
msgid_plural "%d Translation Updates"
msgstr[0] ""
msgstr[1] ""

#: src/WP_Translations/WordPress/Admin/TranslationUpgrader.php:34, src/WP_Translations/WordPress/Admin/Actions/LanguageActions.php:30, src/WP_Translations/WordPress/Admin/Actions/LanguageActions.php:64, src/WP_Translations/WordPress/Admin/Actions/TranslationActions.php:42, src/WP_Translations/WordPress/Admin/Actions/TranslationActions.php:134, src/WP_Translations/WordPress/Admin/Actions/TranslationActions.php:164
msgid "You don&#8217;t have permission to do this."
msgstr ""

#: src/WP_Translations/WordPress/Admin/TranslationUpgrader.php:41
msgid "You do not have sufficient permissions to update this site."
msgstr ""

#: src/WP_Translations/WordPress/Admin/TranslationUpgrader.php:86
msgid "Unable to connect to the filesystem. Please confirm your credentials."
msgstr ""

#: src/WP_Translations/WordPress/Admin/TranslationUpgrader.php:96
msgid "Translations update failed."
msgstr ""

#: src/WP_Translations/WordPress/Admin/UpdateCore.php:47, src/WP_Translations/WordPress/Admin/Page/PageLogs.php:61, src/WP_Translations/WordPress/Admin/Page/PageTranslations.php:91
msgid "Text Domain"
msgstr ""

#: src/WP_Translations/WordPress/Admin/UpdateCore.php:48, src/WP_Translations/WordPress/Helpers/RepositoryHelper.php:341, src/WP_Translations/WordPress/Admin/Page/PageRepositories.php:81
msgid "Type"
msgstr ""

#: src/WP_Translations/WordPress/Admin/UpdateCore.php:49, src/WP_Translations/WordPress/Helpers/PageHelper.php:25, src/WP_Translations/WordPress/Admin/Page/PageSettings.php:32
msgid "Languages"
msgstr ""

#: src/WP_Translations/WordPress/Admin/UpdateCore.php:50
msgid "Current Version"
msgstr ""

#: src/WP_Translations/WordPress/Admin/UpdateCore.php:51
msgid "Available Version"
msgstr ""

#: src/WP_Translations/WordPress/Admin/UpdateCore.php:52
msgid "Source"
msgstr ""

#: src/WP_Translations/WordPress/Admin/UpdateCore.php:53, src/WP_Translations/WordPress/Admin/Actions/CapabilityActions.php:45, src/WP_Translations/WordPress/Admin/Page/PageLanguages.php:93, src/WP_Translations/WordPress/Admin/Page/PagePremium.php:102, src/WP_Translations/WordPress/Admin/Page/PageRepositories.php:86, src/WP_Translations/WordPress/Admin/Page/PageSettings.php:204, src/WP_Translations/WordPress/Admin/Page/PageTranslations.php:106
msgid "Actions"
msgstr ""

#: src/WP_Translations/WordPress/Admin/UpdateCore.php:65, src/WP_Translations/WordPress/Helpers/TranslationHelper.php:318
msgid "Core"
msgstr ""

#: src/WP_Translations/WordPress/Admin/UpdateCore.php:73, src/WP_Translations/WordPress/Helpers/TranslationHelper.php:310
msgid "Plugins"
msgstr ""

#: src/WP_Translations/WordPress/Admin/UpdateCore.php:85, src/WP_Translations/WordPress/Helpers/TranslationHelper.php:314
msgid "Themes"
msgstr ""

#: src/WP_Translations/WordPress/Admin/UpdateCore.php:97, src/WP_Translations/WordPress/Admin/Page/PageTranslations.php:172
msgid "No translations found"
msgstr ""

#: src/WP_Translations/WordPress/Admin/UpdateCore.php:126, src/WP_Translations/WordPress/Helpers/ProductHelper.php:114
msgid "Changelog"
msgstr ""

#: src/WP_Translations/WordPress/Helpers/Helper.php:203
msgid "Install languages"
msgstr ""

#: src/WP_Translations/WordPress/Helpers/Helper.php:204
msgid "Update translations"
msgstr ""

#: src/WP_Translations/WordPress/Helpers/LicenseHelper.php:39
msgid "Activated"
msgstr ""

#: src/WP_Translations/WordPress/Helpers/LicenseHelper.php:44
msgid "Deactivated"
msgstr ""

#: src/WP_Translations/WordPress/Helpers/LicenseHelper.php:49, src/WP_Translations/WordPress/Helpers/LicenseHelper.php:89
msgid "Expired"
msgstr ""

#: src/WP_Translations/WordPress/Helpers/LicenseHelper.php:54
msgid "Inactive Site"
msgstr ""

#: src/WP_Translations/WordPress/Helpers/LicenseHelper.php:59
msgid "Inactive Key"
msgstr ""

#: src/WP_Translations/WordPress/Helpers/LicenseHelper.php:68
msgid "Disabled"
msgstr ""

#: src/WP_Translations/WordPress/Helpers/LicenseHelper.php:74
msgid "Invalid"
msgstr ""

#: src/WP_Translations/WordPress/Helpers/LicenseHelper.php:79
msgid "Activation Limit"
msgstr ""

#: src/WP_Translations/WordPress/Helpers/LicenseHelper.php:84
msgid "Invalid key for this product"
msgstr ""

#: src/WP_Translations/WordPress/Helpers/LicenseHelper.php:106
msgid "License Activated"
msgstr ""

#: src/WP_Translations/WordPress/Helpers/LicenseHelper.php:111, src/WP_Translations/WordPress/Helpers/LicenseHelper.php:148
msgid "Your license key expired on %s"
msgstr ""

#: src/WP_Translations/WordPress/Helpers/LicenseHelper.php:117
msgid "Your license is not active for this site"
msgstr ""

#: src/WP_Translations/WordPress/Helpers/LicenseHelper.php:121
msgid "Inactive License Key"
msgstr ""

#: src/WP_Translations/WordPress/Helpers/LicenseHelper.php:130
msgid "Your license key has been disabled"
msgstr ""

#: src/WP_Translations/WordPress/Helpers/LicenseHelper.php:135
msgid "Invalid license"
msgstr ""

#: src/WP_Translations/WordPress/Helpers/LicenseHelper.php:139
msgid "Your license key has reached its activation limit"
msgstr ""

#: src/WP_Translations/WordPress/Helpers/LicenseHelper.php:143
msgid "This appears to be an invalid license key for this product"
msgstr ""

#: src/WP_Translations/WordPress/Helpers/LoggerHelper.php:23
msgid "Activation"
msgstr ""

#: src/WP_Translations/WordPress/Helpers/LoggerHelper.php:28
msgid "Deactivation"
msgstr ""

#: src/WP_Translations/WordPress/Helpers/LoggerHelper.php:33, src/WP_Translations/WordPress/Helpers/ModalHelper.php:213, src/WP_Translations/WordPress/Helpers/ModalHelper.php:326
msgid "Save"
msgstr ""

#: src/WP_Translations/WordPress/Helpers/LoggerHelper.php:38, src/WP_Translations/WordPress/Admin/Actions/LanguageActions.php:42, src/WP_Translations/WordPress/Admin/Page/PageLanguages.php:145, src/WP_Translations/WordPress/Admin/Page/PageRepositories.php:174
msgid "Delete"
msgstr ""

#: src/WP_Translations/WordPress/Helpers/LoggerHelper.php:40
msgid "Deleted"
msgstr ""

#: src/WP_Translations/WordPress/Helpers/LoggerHelper.php:44
msgid "Updated"
msgstr ""

#: src/WP_Translations/WordPress/Helpers/LoggerHelper.php:46, src/WP_Translations/WordPress/Helpers/LoggerHelper.php:60
msgid "Success"
msgstr ""

#: src/WP_Translations/WordPress/Helpers/LoggerHelper.php:50
msgid "Update available"
msgstr ""

#: src/WP_Translations/WordPress/Helpers/LoggerHelper.php:52
msgid "Waiting"
msgstr ""

#: src/WP_Translations/WordPress/Helpers/ModalHelper.php:42, src/WP_Translations/WordPress/Helpers/ModalHelper.php:43, src/WP_Translations/WordPress/Helpers/ModalHelper.php:130, src/WP_Translations/WordPress/Helpers/ModalHelper.php:131, src/WP_Translations/WordPress/Helpers/ModalHelper.php:241, src/WP_Translations/WordPress/Helpers/ModalHelper.php:242, src/WP_Translations/WordPress/Admin/Page/PageTranslations.php:225, src/WP_Translations/WordPress/Admin/Page/PageTranslations.php:225
msgid "Close modal"
msgstr ""

#: src/WP_Translations/WordPress/Helpers/ModalHelper.php:61
msgid "Version:"
msgstr ""

#: src/WP_Translations/WordPress/Helpers/ModalHelper.php:64
msgid "Last Update:"
msgstr ""

#: src/WP_Translations/WordPress/Helpers/ModalHelper.php:68
msgid "Contributors:"
msgstr ""

#: src/WP_Translations/WordPress/Helpers/ModalHelper.php:94
msgid "Go to Store"
msgstr ""

#: src/WP_Translations/WordPress/Helpers/ModalHelper.php:110, src/WP_Translations/WordPress/Helpers/PageHelper.php:35, src/WP_Translations/WordPress/Admin/Page/PageSettings.php:295, src/WP_Translations/WordPress/Admin/Page/PageTranslations.php:225
msgid "Settings"
msgstr ""

#: src/WP_Translations/WordPress/Helpers/ModalHelper.php:140, src/WP_Translations/WordPress/Helpers/ModalHelper.php:251
msgid "Loading settings"
msgstr ""

#: src/WP_Translations/WordPress/Helpers/PageHelper.php:20
msgid "Translations"
msgstr ""

#: src/WP_Translations/WordPress/Helpers/ProductHelper.php:118
msgid "Installation"
msgstr ""

#: src/WP_Translations/WordPress/Helpers/ProductHelper.php:122, src/WP_Translations/WordPress/Helpers/RepositoryHelper.php:380, src/WP_Translations/WordPress/Admin/Page/PageRepositories.php:76, src/WP_Translations/WordPress/Admin/Page/PageSettings.php:199
msgid "Description"
msgstr ""

#: src/WP_Translations/WordPress/Helpers/RepositoryHelper.php:322, src/WP_Translations/WordPress/Admin/Page/PagePremium.php:82, src/WP_Translations/WordPress/Admin/Page/PageRepositories.php:71, src/WP_Translations/WordPress/Admin/Page/PageTranslations.php:86
msgid "Name"
msgstr ""

#: src/WP_Translations/WordPress/Helpers/RepositoryHelper.php:329
msgid "Provider"
msgstr ""

#: src/WP_Translations/WordPress/Helpers/RepositoryHelper.php:344
msgid "Public"
msgstr ""

#: src/WP_Translations/WordPress/Helpers/RepositoryHelper.php:345
msgid "Private"
msgstr ""

#: src/WP_Translations/WordPress/Helpers/RepositoryHelper.php:356
msgid "Token"
msgstr ""

#: src/WP_Translations/WordPress/Helpers/RepositoryHelper.php:366
msgid "URL"
msgstr ""

#: src/WP_Translations/WordPress/Helpers/RepositoryHelper.php:373
msgid "Logo"
msgstr ""

#: src/WP_Translations/WordPress/Helpers/TranslationHelper.php:332
msgid "Allow updates from"
msgstr ""

#: src/WP_Translations/WordPress/Helpers/TranslationHelper.php:340
msgid "Lock translation"
msgstr ""

#: src/WP_Translations/WordPress/Helpers/TranslationHelper.php:347
msgid "Custom translation"
msgstr ""

#: src/WP_Translations/WordPress/Helpers/TranslationHelper.php:354
msgid "Upload custom file (mo/po)"
msgstr ""

#: src/WP_Translations/WordPress/Admin/Actions/CapabilityActions.php:35
msgid "Roles"
msgstr ""

#: src/WP_Translations/WordPress/Admin/Actions/CapabilityActions.php:40, src/WP_Translations/WordPress/Admin/Actions/CapabilityActions.php:58, src/WP_Translations/WordPress/Admin/Actions/CapabilityActions.php:73, src/WP_Translations/WordPress/Admin/Page/PageSettings.php:61
msgid "Capabilities"
msgstr ""

#: src/WP_Translations/WordPress/Admin/Actions/CapabilityActions.php:60, src/WP_Translations/WordPress/Admin/Page/PageSettings.php:63
msgid "Add translation capabilities (install/update) to specific roles."
msgstr ""

#: src/WP_Translations/WordPress/Admin/Actions/PerformanceActions.php:29
msgid "MO Cache"
msgstr ""

#: src/WP_Translations/WordPress/Admin/Actions/PerformanceActions.php:31
msgid "Cache translations with object cache"
msgstr ""

#: src/WP_Translations/WordPress/Admin/Actions/PerformanceActions.php:36
msgid "Gettext"
msgstr ""

#: src/WP_Translations/WordPress/Admin/Actions/PerformanceActions.php:39
msgid "WordPress gettext"
msgstr ""

#: src/WP_Translations/WordPress/Admin/Actions/PerformanceActions.php:40
msgid "PHP native gettext"
msgstr ""

#: src/WP_Translations/WordPress/Admin/Actions/PerformanceActions.php:42
msgid "Select Gettext library."
msgstr ""

#: src/WP_Translations/WordPress/Admin/Actions/PerformanceActions.php:54, src/WP_Translations/WordPress/Admin/Page/PageSettings.php:68
msgid "Performance"
msgstr ""

#: src/WP_Translations/WordPress/Admin/Actions/PremiumActions.php:57, src/WP_Translations/WordPress/Admin/Actions/PremiumActions.php:68, src/WP_Translations/WordPress/Admin/Page/PagePremium.php:258
msgid "Licenses"
msgstr ""

#: src/WP_Translations/WordPress/Admin/Actions/PremiumActions.php:80, src/WP_Translations/WordPress/Admin/Page/PageLogs.php:56
msgid "Date"
msgstr ""

#: src/WP_Translations/WordPress/Admin/Actions/PremiumActions.php:85, src/WP_Translations/WordPress/Admin/Page/PagePremium.php:87
msgid "License Key"
msgstr ""

#: src/WP_Translations/WordPress/Admin/Actions/PremiumActions.php:90, src/WP_Translations/WordPress/Admin/Page/PageLogs.php:76
msgid "Action"
msgstr ""

#: src/WP_Translations/WordPress/Admin/Actions/PremiumActions.php:95, src/WP_Translations/WordPress/Admin/Page/PagePremium.php:97
msgid "Status"
msgstr ""

#: src/WP_Translations/WordPress/Admin/Actions/PremiumActions.php:108
msgid "Product Updates"
msgstr ""

#: src/WP_Translations/WordPress/Admin/Actions/PremiumActions.php:110
msgid "Force product updates."
msgstr ""

#: src/WP_Translations/WordPress/Admin/Actions/PremiumActions.php:124, src/WP_Translations/WordPress/Admin/Actions/PremiumActions.php:160, src/WP_Translations/WordPress/Admin/Actions/PremiumActions.php:190, src/WP_Translations/WordPress/Admin/Actions/PremiumActions.php:242, src/WP_Translations/WordPress/Admin/Actions/PremiumActions.php:262, src/WP_Translations/WordPress/Admin/Actions/PremiumActions.php:325, src/WP_Translations/WordPress/Admin/Actions/RepositoryActions.php:110, src/WP_Translations/WordPress/Admin/Actions/RepositoryActions.php:133, src/WP_Translations/WordPress/Admin/Actions/RepositoryActions.php:156, src/WP_Translations/WordPress/Admin/Actions/RepositoryActions.php:177, src/WP_Translations/WordPress/Admin/Actions/RepositoryActions.php:257, src/WP_Translations/WordPress/Admin/Actions/RepositoryActions.php:309
msgid "An error has occurred."
msgstr ""

#: src/WP_Translations/WordPress/Admin/Actions/PremiumActions.php:128
msgid "Please, enter a license key."
msgstr ""

#: src/WP_Translations/WordPress/Admin/Actions/PremiumActions.php:146
msgid "License key successfully saved."
msgstr ""

#: src/WP_Translations/WordPress/Admin/Actions/PremiumActions.php:178
msgid "License key successfully deleted."
msgstr ""

#: src/WP_Translations/WordPress/Admin/Actions/PremiumActions.php:208
msgid "License successfully activated."
msgstr ""

#: src/WP_Translations/WordPress/Admin/Actions/PremiumActions.php:282
msgid "License successfully deactivated."
msgstr ""

#: src/WP_Translations/WordPress/Admin/Actions/PremiumActions.php:289
msgid "License fails to be deactivated."
msgstr ""

#: src/WP_Translations/WordPress/Admin/Actions/PremiumActions.php:333
msgid "Check for updates done."
msgstr ""

#: src/WP_Translations/WordPress/Admin/Actions/RepositoryActions.php:119
msgid "Repository activated."
msgstr ""

#: src/WP_Translations/WordPress/Admin/Actions/RepositoryActions.php:142
msgid "Repository deactivated."
msgstr ""

#: src/WP_Translations/WordPress/Admin/Actions/RepositoryActions.php:165
msgid "Repository settings loaded"
msgstr ""

#: src/WP_Translations/WordPress/Admin/Actions/RepositoryActions.php:187, src/WP_Translations/WordPress/Admin/Actions/RepositoryActions.php:194
msgid "This field is required."
msgstr ""

#: src/WP_Translations/WordPress/Admin/Actions/RepositoryActions.php:201
msgid "HTTP repository must be public!"
msgstr ""

#: src/WP_Translations/WordPress/Admin/Actions/RepositoryActions.php:208
msgid "Token is required for private repository!"
msgstr ""

#: src/WP_Translations/WordPress/Admin/Actions/RepositoryActions.php:230
msgid "Something went wrong"
msgstr ""

#: src/WP_Translations/WordPress/Admin/Actions/RepositoryActions.php:243
msgid "Repository successfully added!"
msgstr ""

#: src/WP_Translations/WordPress/Admin/Actions/RepositoryActions.php:243
msgid "Settings saved."
msgstr ""

#: src/WP_Translations/WordPress/Admin/Actions/RepositoryActions.php:269
msgid "Repository successfully deleted!"
msgstr ""

#: src/WP_Translations/WordPress/Admin/Actions/SettingsActions.php:50, src/WP_Translations/WordPress/Admin/Actions/SettingsActions.php:73
msgid "Trying to cheat or something?"
msgstr ""

#: src/WP_Translations/WordPress/Admin/Actions/SettingsActions.php:50, src/WP_Translations/WordPress/Admin/Actions/SettingsActions.php:73
msgid "Error"
msgstr ""

#: src/WP_Translations/WordPress/Admin/Actions/TranslationActions.php:94
msgid "Please select a file"
msgstr ""

#: src/WP_Translations/WordPress/Admin/Actions/TranslationActions.php:114
msgid "Settings saved!"
msgstr ""

#: src/WP_Translations/WordPress/Admin/Actions/TranslationActions.php:174
msgid "Translations settings loaded"
msgstr ""

#: src/WP_Translations/WordPress/Admin/Page/PageLanguages.php:23
msgid "Install"
msgstr ""

#: src/WP_Translations/WordPress/Admin/Page/PageLanguages.php:78
msgid "Locales"
msgstr ""

#: src/WP_Translations/WordPress/Admin/Page/PageLanguages.php:83
msgid "Native Names"
msgstr ""

#: src/WP_Translations/WordPress/Admin/Page/PageLanguages.php:88
msgid "English Names"
msgstr ""

#: src/WP_Translations/WordPress/Admin/Page/PageLanguages.php:163
msgid "Available Translations"
msgstr ""

#: src/WP_Translations/WordPress/Admin/Page/PageLanguages.php:167
msgid "Available Languages"
msgstr ""

#: src/WP_Translations/WordPress/Admin/Page/PageLogs.php:66
msgid "Language"
msgstr ""

#: src/WP_Translations/WordPress/Admin/Page/PageLogs.php:71
msgid "Version"
msgstr ""

#: src/WP_Translations/WordPress/Admin/Page/PagePremium.php:30
msgid "Available translations"
msgstr ""

#: src/WP_Translations/WordPress/Admin/Page/PagePremium.php:39
msgid "Addons"
msgstr ""

#: src/WP_Translations/WordPress/Admin/Page/PagePremium.php:92
msgid "Activations"
msgstr ""

#: src/WP_Translations/WordPress/Admin/Page/PagePremium.php:201, src/WP_Translations/WordPress/Admin/Page/PagePremium.php:201
msgid "Save license"
msgstr ""

#: src/WP_Translations/WordPress/Admin/Page/PagePremium.php:202, src/WP_Translations/WordPress/Admin/Page/PagePremium.php:202
msgid "Get a license"
msgstr ""

#: src/WP_Translations/WordPress/Admin/Page/PagePremium.php:237, src/WP_Translations/WordPress/Admin/Page/PagePremium.php:237, src/WP_Translations/WordPress/Admin/Page/PagePremium.php:237
msgid "Delete license"
msgstr ""

#: src/WP_Translations/WordPress/Admin/Page/PagePremium.php:262
msgid "Available Products"
msgstr ""

#: src/WP_Translations/WordPress/Admin/Page/PageRepositories.php:132
msgid "WP-Translations community repository"
msgstr ""

#: src/WP_Translations/WordPress/Admin/Page/PageRepositories.php:171
msgid "Protected"
msgstr ""

#: src/WP_Translations/WordPress/Admin/Page/PageSettings.php:22
msgid "Features"
msgstr ""

#: src/WP_Translations/WordPress/Admin/Page/PageSettings.php:27
msgid "UI Integration"
msgstr ""

#: src/WP_Translations/WordPress/Admin/Page/PageSettings.php:42
msgid "Advanced"
msgstr ""

#: src/WP_Translations/WordPress/Admin/Page/PageSettings.php:70
msgid "Optimize localization."
msgstr ""

#: src/WP_Translations/WordPress/Admin/Page/PageSettings.php:75
msgid "Premium"
msgstr ""

#: src/WP_Translations/WordPress/Admin/Page/PageSettings.php:77
msgid "Get Premium translation updates and notices."
msgstr ""

#: src/WP_Translations/WordPress/Admin/Page/PageSettings.php:83
msgid "Allow external repositories for translation updates."
msgstr ""

#: src/WP_Translations/WordPress/Admin/Page/PageSettings.php:89
msgid "Core updates"
msgstr ""

#: src/WP_Translations/WordPress/Admin/Page/PageSettings.php:91
msgid "Display translation updates details in core's update page."
msgstr ""

#: src/WP_Translations/WordPress/Admin/Page/PageSettings.php:95
msgid "Plugins updates"
msgstr ""

#: src/WP_Translations/WordPress/Admin/Page/PageSettings.php:97
msgid "Display translation updates details in plugin's update page."
msgstr ""

#: src/WP_Translations/WordPress/Admin/Page/PageSettings.php:101
msgid "Themes updates"
msgstr ""

#: src/WP_Translations/WordPress/Admin/Page/PageSettings.php:103
msgid "Display translation updates details in theme's update page."
msgstr ""

#: src/WP_Translations/WordPress/Admin/Page/PageSettings.php:107
msgid "Display bubble count"
msgstr ""

#: src/WP_Translations/WordPress/Admin/Page/PageSettings.php:109
msgid "Updates bubble count with translations."
msgstr ""

#: src/WP_Translations/WordPress/Admin/Page/PageSettings.php:113
msgid "Page position"
msgstr ""

#: src/WP_Translations/WordPress/Admin/Page/PageSettings.php:116
msgid "Dedicated page"
msgstr ""

#: src/WP_Translations/WordPress/Admin/Page/PageSettings.php:117
msgid "Settings subpage"
msgstr ""

#: src/WP_Translations/WordPress/Admin/Page/PageSettings.php:119
msgid "Select admin page position"
msgstr ""

#: src/WP_Translations/WordPress/Admin/Page/PageSettings.php:125
msgid "Front User Locale"
msgstr ""

#: src/WP_Translations/WordPress/Admin/Page/PageSettings.php:127
msgid "Enable user locale on front-end"
msgstr ""

#: src/WP_Translations/WordPress/Admin/Page/PageSettings.php:131
msgid "Language Fallback"
msgstr ""

#: src/WP_Translations/WordPress/Admin/Page/PageSettings.php:133
msgid "Set a fallback for your chosen language"
msgstr ""

#: src/WP_Translations/WordPress/Admin/Page/PageSettings.php:140
msgid "Async updates"
msgstr ""

#: src/WP_Translations/WordPress/Admin/Page/PageSettings.php:142
msgid "Enable/disable async updates"
msgstr ""

#: src/WP_Translations/WordPress/Admin/Page/PageSettings.php:148
msgid "Debug Mode"
msgstr ""

#: src/WP_Translations/WordPress/Admin/Page/PageSettings.php:150
msgid "Toggle debug mode. It may affect performance."
msgstr ""

#: src/WP_Translations/WordPress/Admin/Page/PageSettings.php:154
msgid "Check Translation Updates"
msgstr ""

#: src/WP_Translations/WordPress/Admin/Page/PageSettings.php:156
msgid "Force translation updates."
msgstr ""

#: src/WP_Translations/WordPress/Admin/Page/PageSettings.php:160
msgid "Reset Translations"
msgstr ""

#: src/WP_Translations/WordPress/Admin/Page/PageSettings.php:162
msgid "Reset all translations settings: repositories priorities, locked translations..."
msgstr ""

#: src/WP_Translations/WordPress/Admin/Page/PageSettings.php:166
msgid "Purge Logs"
msgstr ""

#: src/WP_Translations/WordPress/Admin/Page/PageSettings.php:194
msgid "Options"
msgstr ""

#: src/WP_Translations/WordPress/Admin/Page/PageSettings.php:284
msgid "Save Settings"
msgstr ""

#: src/WP_Translations/WordPress/Admin/Page/PageTranslations.php:34
msgid "Switch"
msgstr ""

#: src/WP_Translations/WordPress/Admin/Page/PageTranslations.php:96
msgid "Loaded Translations"
msgstr ""

#: src/WP_Translations/WordPress/Admin/Page/PageTranslations.php:101
msgid "Authorized Repositories"
msgstr ""

#: src/WP_Translations/WordPress/Admin/Page/PageTranslations.php:158
msgctxt "text-domain"
msgid "not loaded"
msgstr ""

#: src/WP_Translations/WordPress/Admin/Page/PageTranslations.php:222, src/WP_Translations/WordPress/Admin/Page/PageTranslations.php:222
msgid "Update"
msgstr ""

#: src/WP_Translations/WordPress/Admin/Page/PageTranslations.php:244
msgid "Translations Updates"
msgstr ""

#: src/WP_Translations/WordPress/Admin/Page/PageTranslations.php:248
msgid "Translations Settings"
msgstr ""
