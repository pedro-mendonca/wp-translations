<?php
/**
 * WP-Translations
 *
 * @package     WP-Translations
 * @author      WP-Translations Team
 * @version     1.0.8
 *
 * @copyright   2017 WP-Translations Team
 * @license     http://creativecommons.org/licenses/GPL/2.0/ GNU General Public License, version 2 or higher
 *
 * @wordpress-plugin
 * Plugin Name: WP-Translations
 * Description: WP-Translations add external translations repositories and extra translations features.

 * Version:     1.0.8
 * Author:      WP-Translations Team
 * Author URI:  https://wp-translations.org
 * Text Domain: wp-translations
 * Domain Path: /languages
 * Copyright:   2017 WP-Translations Team
 */

defined( 'ABSPATH' ) or die( 'You don&#8217;t have permission to do this.' );

require_once( dirname( __FILE__ ) . '/vendor/autoload.php' );

define( 'WPTORG_VERSION',                      '1.0.8' );
define( 'WPTORG_BASE_FILE',                     plugin_basename( __FILE__ ) );
define( 'WPTORG_SLUG',                          'wp-translations' );
define( 'WPTORG_PLUGIN_NAME',                   'WP-Translations' );
define( 'WPTORG_PLUGIN_PATH',                   plugin_dir_path( __FILE__ ) );
define( 'WPTORG_PLUGIN_URL',                    plugin_dir_url( __FILE__ ) );
define( 'WPTORG_PLUGIN_DIR_TEMPLATES',          WPTORG_PLUGIN_PATH . 'templates' );
define( 'WPTORG_PLUGIN_DIR_TEMPLATES_ADMIN',    WPTORG_PLUGIN_DIR_TEMPLATES . '/admin' );
define( 'WPTORG_CONTENT_DIR',                   WP_CONTENT_DIR . '/wp-translations' );
define( 'WPTORG_CONTENT_PATH_TEMP',             WP_CONTENT_DIR . '/wp-translations/temp' );
define( 'WPTORG_CONTENT_URL_TEMP',              WP_CONTENT_URL . '/wp-translations/temp' );

use WP_Translations\Models\HooksInterface;
use WP_Translations\Models\HooksAdminInterface;
use WP_Translations\Models\HooksFrontInterface;
use WP_Translations\Models\ActivationInterface;

use WP_Translations\WordPress\Admin\EnqueueAssets;
use WP_Translations\WordPress\Admin\Notice;

use WP_Translations\WordPress\Admin\Actions\CapabilityActions;
use WP_Translations\WordPress\Admin\Actions\LanguageActions;
use WP_Translations\WordPress\Admin\Actions\RepositoryActions;
use WP_Translations\WordPress\Admin\Actions\PremiumActions;
use WP_Translations\WordPress\Admin\Actions\PerformanceActions;
use WP_Translations\WordPress\Admin\Actions\TranslationActions;
use WP_Translations\WordPress\Admin\Actions\SettingsActions;
use WP_Translations\WordPress\Admin\Actions\PluginActions;

use WP_Translations\WordPress\Admin\TranslationSetup;
use WP_Translations\WordPress\Admin\TranslationUpdater;
use WP_Translations\WordPress\Admin\TranslationUpgrader;
use WP_Translations\WordPress\Admin\UpdateCore;
use WP_Translations\WordPress\Admin\Page;

use WP_Translations\WordPress\Admin\PluginUpdater;
use WP_Translations\ThirdParty\ElegantThemes;

// Front
use WP_Translations\WordPress\Front\UserLocale;

/**
 * WP_Translations
 *
 * @author Jerome Sadler
 * @version 1.0.0
 * @since 1.0.0
 */
class WP_Translations implements HooksInterface {

  protected $actions = array();
  protected $slug = WPTORG_SLUG;

  /**
   * @param array $actions
   */
  public function __construct( $actions = array() ) {
    $this->actions = $actions;
  }

  /**
   * @return boolean
   */
  protected function canBeLoaded() {
    return true;
  }


  /**
   * Execute plugin
   */
  public function execute() {

    if ( $this->canBeLoaded() ) {
      add_action( 'plugins_loaded',         array( $this, 'hooks' ), 0 );
      register_activation_hook(__FILE__,    array( $this, 'executePlugin') );
    }
  }

  /**
   * @return array
   */
  public function getActions() {
    return $this->actions;
  }

  /**
   * Implements hooks from HooksInterface
   *
   * @see WP_Translations\Models\HooksInterface
   *
   * @return void
   */
  public function hooks() {

    foreach ( $this->getActions() as $key => $action ) {

      switch( true ) {
        case $action instanceof HooksAdminInterface:
          if ( is_admin() ) {
            $action->hooks();
          }
          break;
        case $action instanceOf HooksFrontInterface:
          if (!is_admin()) {
            $action->hooks();
          }
          break;
        case $action instanceof HooksInterface:
          $action->hooks();
          break;
      }

    }

  }

  public function executePlugin() {

    switch ( current_filter() ) {
      case 'activate_' . $this->slug . '/' . $this->slug . '.php':
          foreach ( $this->getActions() as $key => $action ) {
              if ( $action instanceOf ActivationInterface ) {
                  $action->activation();
              }
          }
          break;
    }

  }

}

$actions = apply_filters( 'wp_translations_actions', array(
    new EnqueueAssets(),
    new Notice(),
    new CapabilityActions(),
    new LanguageActions(),
    new RepositoryActions(),
    new PremiumActions(),
    new PerformanceActions(),
    new TranslationActions(),
    new SettingsActions(),
    new TranslationSetup(),
    new TranslationUpdater(),
    new TranslationUpgrader(),
    new UpdateCore(),
    new Page(),
    new PluginActions(),
    new PluginUpdater(),
    new ElegantThemes(),
    new UserLocale()
));

$wp_translations = new WP_Translations( $actions );
$wp_translations->execute();


/**
 * Delete Options
 */
function wptDeactivationHook() {

}
register_deactivation_hook( __FILE__, 'wptDeactivationHook' );
