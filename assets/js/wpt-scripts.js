$( function( $ ) {

  /*
   * ----------------------
   * Global
   * ----------------------
   */

  // Get URL Params
  var urlParam = function(name){
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if ( results !== null ) {
      return results[1] || 0;
    }
  }

  // Get Current Page Slug
  var currentPage = urlParam('wpt-page');

  // Menu Highlight
  $( '#toplevel_page_wp-translations .wp-submenu li' ).each(function(i) {
    var link = $(this).find('a');
    $(this).removeClass( 'current' );
    if ( 'admin.php?page=wp-translations&wpt-page=' + currentPage == $( link ).attr('href')  ) {
      $(this).addClass( 'current' );
    }
  });

  // Hide / Show table actions
  var location = $( '.wpt-settings-tabs__link[aria-selected="true"]' ).attr('aria-controls');
  $( '#wpt-table-actions-' + location ).removeClass( 'hidden' );

  $( '#wpt-settings-form .wpt-settings-tabs__link').live( "click", function (e) {
    e.preventDefault();
    $( "[id^=wpt-table-actions-]" ).addClass( 'hidden' );
    var location = $( this ).attr('aria-controls');
    $( '#wpt-table-actions-' + location ).removeClass( 'hidden' );
  });

  // Custom Link For Help Tabs
  $( ".wpt-help-link" ).live( "click", function(e) {
    e.preventDefault();

    var tab = $( this ).attr('data-tab');

    $('li[id^="tab-link-"]').each(function(){
      $(this).removeClass('active');
    });

    // Hide all panels
    $('div[id^="tab-panel-"]').each(function(){
        $(this).css('display','none');
    });

    $( '#tab-link-' + tab ).addClass('active');
    $( '#tab-panel-' + tab ).css('display','block');

    $( '#js-modal-close' ).click();
    $('#contextual-help-link').click();
  });

  // DataTable Configuation
  $('#wpt-logs-license-table, #wpt-logs-update-table').DataTable({
       "pagingType"  : "simple",
       "lengthChange": false,
       "searching"   : false,
       "order"       : [[ 0, "desc" ]],
       "language"    : {
        "sProcessing"    : wpt_license_ajax.i18n.datatable.sProcessing,
        "sSearch"        : wpt_license_ajax.i18n.datatable.sSearch,
        "sLengthMenu"    : wpt_license_ajax.i18n.datatable.sLengthMenu,
        "sInfo"          : wpt_license_ajax.i18n.datatable.sInfo,
        "sInfoEmpty"     : wpt_license_ajax.i18n.datatable.sInfoEmpty,
        "sInfoFiltered"  : wpt_license_ajax.i18n.datatable.sInfoFiltered,
        "sLoadingRecords": wpt_license_ajax.i18n.datatable.sLoadingRecords,
        "sZeroRecords"   : wpt_license_ajax.i18n.datatable.sZeroRecords,
        "sEmptyTable"    : wpt_license_ajax.i18n.datatable.sEmptyTable,
        "oPaginate"      : {
            "sFirst"   : wpt_license_ajax.i18n.datatable.sFirst,
            "sPrevious": wpt_license_ajax.i18n.datatable.sPrevious,
            "sNext"    : wpt_license_ajax.i18n.datatable.sNext,
            "sLast"    : wpt_license_ajax.i18n.datatable.sLast,
        },
        "oAria": {
            "sSortAscending" :  wpt_license_ajax.i18n.datatable.sSortAscending,
            "sSortDescending": wpt_license_ajax.i18n.datatable.sSortDescending,
        }
      }
   });

  // Readme Toggle
  $( "[id^=wpt-readme-]" ).live( "click", function(e) {
    e.preventDefault();

    var slug    = $( this ).attr( 'data-slug' );
    var locale  = $( this ).attr( 'data-locale' );

    $( '#readme-' + locale + '-' + slug ).toggle();
  });

  /*
   * ----------------------
   * Feature : Premium
   * ----------------------
   */

  // Premium Spinner
  var spinner = function ( slug, locale, type ) {
    html = '<span id="wpt-spinner-' + locale + '-'+ slug + '" class="wpt-spinner is-' + type + ' alignright"></span>';
    return html;
  }

  // Premium Button Save
  var save_button = function( slug, name, locale, type ) {
    html = '<button type="button" class="wpt-button" data-type="'+ type + '" data-locale="' + locale + '" data-slug="' + slug + '" data-name="' + name + '" title="' + wpt_license_ajax.i18n.btn.save + '" id="save-license-' + slug + '"><span class="dashicons dashicons-plus-alt"></span>' + wpt_license_ajax.i18n.btn.save + '</button>';
    return html;
  };

  // Premium Button Delete
  var delete_button = function( slug, name, locale, type ) {
    html = '<button type="button" class="wpt-button danger" data-type="'+ type + '" data-locale="' + locale + '" data-slug="' + slug + '" data-name="' + name + '" title="' + wpt_license_ajax.i18n.btn.deleted + '" id="delete-license-' + slug + '"><span class="dashicons dashicons-dismiss"></span><span class="screen-reader-text">' + wpt_license_ajax.i18n.btn.deleted + '</span></button>';
    return html;
  };

  // Premium Button Activate
  var activate_button = function( slug, name, locale, type ) {
    html = '<button type="button" class="wpt-button" data-type="'+ type + '" data-locale="' + locale + '" data-slug="' + slug + '" data-name="' + name + '" title="' + wpt_license_ajax.i18n.btn.activate + '" id="activate-license-' + slug + '"><span class="dashicons dashicons-controls-play"></span> ' + wpt_license_ajax.i18n.btn.activate + '</button>';
    return html;
  };

  // Premium Button Deactivate
  var deactivate_button = function( slug, name, locale, type ) {
    html = '<button type="button" class="wpt-button" data-type="'+ type + '" data-locale="' + locale + '" data-slug="' + slug + '" data-name="' + name + '" title="' + wpt_license_ajax.i18n.btn.deactivate + '" id="deactivate-license-' + slug + '"><span class="dashicons dashicons-controls-pause"></span> ' + wpt_license_ajax.i18n.btn.deactivate + '</button>';
    return html;
  };

  // Premium Button Check
  var check_button = function( slug, name, locale, type ) {
    html = '<button type="button" class="wpt-button" data-type="'+ type + '" data-locale="' + locale + '" data-slug="' + slug + '" data-name="' + name + '" title="' + wpt_license_ajax.i18n.btn.check + '" id="check-license-' + slug + '"><span class="dashicons dashicons-update"></span><span class="screen-reader-text">' + wpt_license_ajax.i18n.btn.check + '</span></button>';
    return html;
  };

  // Premium Renew link
  var renew_link = function( slug, locale, license, data ) {
    html = '<a href="' + wpt_license_ajax.stores[locale].url + wpt_license_ajax.stores[locale].checkout +'?edd_license_key=' + license + '&download_id=' + wpt_license_ajax.products[locale][slug].id + '" id="wpt-renew-' + locale + '-' + slug + '" class="wpt-button" target="_blank"><span class="dashicons dashicons-image-rotate"></span> ' + wpt_license_ajax.i18n.btn.renew + '</a>';
    return html;
  }

  // Premium Upgrade Link
  var upgrade_link = function( slug, locale, license, data, upgrade_id ) {
    html = '<a href="' + wpt_license_ajax.stores[locale].url + wpt_license_ajax.stores[locale].checkout +'?edd_action=sl_license_upgrade&license_id=' + data.license_id + '&upgrade_id=' + upgrade_id + '" id="wpt-upgrade-' + locale + '-' + slug + '" class="wpt-button" target="_blank"><span class="dashicons dashicons-unlock"></span> ' + wpt_license_ajax.i18n.btn.upgrade + '</a>';
    return html;
  }

  // Premium Button Install
  var install_button = function( slug, name, locale, type ) {
    html = '<button type=button class="wpt-button wpt-install-package" data-type="' + type + '" data-locale="' + locale + '" data-slug="' + slug + '" data-name="' + name + '" ><span class="dashicons dashicons-download"></span> ' + wpt_license_ajax.i18n.btn.download + '</button>';
    return html;
  }

  // Defaut notices values
  var notice  = 'info';
  var color   = '#00A0D2';

  // Premium Save License
  $( "[id^=save-license-]" ).live( "click", function(e) {
    e.preventDefault();

    var name    = $( this ).attr( 'data-name' );
    var slug    = $( this ).attr( 'data-slug' );
    var locale  = $( this ).attr( 'data-locale' );
    var type    = $( this ).attr( 'data-type' );
    var license = $( '#wpt-license-' + locale + '-' + slug ).val();

    $.ajax({
      type: "POST",
      url : wpt_license_ajax.ajaxurl,
      data: {
        'action'  : 'saveLicense',
        'nonce'   : wpt_license_ajax.nonce,
        'name'    : name,
        'slug'    : slug,
        'type'    : type,
        'locale'  : locale,
        'license' : license,
      },
      beforeSend: function(response) {
        $( '#wpt-spinner-' + locale + '-' + slug ).addClass( 'is-active' );
        $( '#wpt-row-notice-' + locale + '-' + slug )
          .show()
          .html( '<td colspan="6" class="wpt-notice-table wpt-notice-table-' + notice + '">' + spinner( slug, locale, notice ) + wpt_license_ajax.i18n.messages.saving + '</td>' )
      },
    })

    .done( function( response, textStatus, jqXHR ) {

      if ( true === response.success ) {
        var notice = 'success';
        var color  = '#46b450';

        $( '#wpt-col-actions-' + locale + '-' + slug ).html(
          activate_button( slug, name, locale, type ) +
          delete_button( slug, name, locale, type )
        );
      } else {
        var notice = 'error';
        var color = '#DC3232';

      }
      $( '#wpt-spinner-' + locale + '-' + slug ).removeClass( 'is-active' ).addClass( 'is-' + notice );
      $( '#wpt-row-notice-' + locale + '-' + slug )
        .addClass( 'is-' + notice )
        .html( '<td colspan="6" class="wpt-notice-table wpt-notice-table-' + notice + '">' + spinner( slug, locale, notice ) + response.data.message + '</td>' ).fadeOut(1000);
    });
  });

  // Premium Delete License
  $( "[id^=delete-license-]" ).live( "click", function(e) {
    e.preventDefault();

    var slug    = $( this ).attr( 'data-slug' );
    var name    = $( this ).attr( 'data-name' );
    var type    = $( this ).attr( 'data-type' );
    var locale  = $( this ).attr( 'data-locale' );
    var license = $( '#wpt-license-' + locale + '-' + slug ).val();

    $.ajax({
      type: "POST",
      url : wpt_license_ajax.ajaxurl,
      data: {
        'action'  : 'deleteLicense',
        'nonce'   : wpt_license_ajax.nonce,
        'slug'    : slug,
        'type'    : type,
        'locale'  : locale,
        'license' : license,
      },
      beforeSend: function(response) {
        $( '#wpt-spinner-' + locale + '-' + slug ).addClass( 'is-active' );
        $( '#wpt-row-notice-' + locale + '-' + slug )
          .show()
          .html( '<td colspan="6" class="wpt-notice-table wpt-notice-table-' + notice + '">' + spinner( slug, locale, notice ) + wpt_license_ajax.i18n.messages.deleting + '</td>' );
      },
    })

    .done( function( response, textStatus, jqXHR ) {

      $( '#wpt-spinner-' + locale + '-' + slug ).removeClass( 'is-active' ).addClass( 'is-success' );
      $( '#wpt-license-' + locale + '-' + slug ).val('');
      $( '#wpt-col-actions-' + locale + '-' + slug ).html( save_button( slug, name, locale, type ) );
      $( '#wpt-col-status-' + locale + '-' + slug ).html('');
      $( '#wpt-row-notice-' + locale + '-' + slug ).html( '<td colspan="6" class="wpt-notice-table wpt-notice-table-success">' + spinner( slug, locale, notice ) + response.data.message + '</td>' );
    })
    .then( function( response, textStatus, jqXHR ) {

      var notice = 'info';
      var color  = '#00A0D2';

      $.ajax({
        type: "POST",
        url: wpt_license_ajax.ajaxurl,
        data: {
          'action': 'clearCacheUpdate',
          'nonce'  : wpt_license_ajax.nonce,
          'type'   : type,
        },
        beforeSend: function(response2) {
          $( '#wpt-row-notice-' + locale + '-' + slug ).html( '<td colspan="6" class="wpt-notice-table wpt-notice-table-' + notice + '">' + spinner( slug, locale, notice ) + wpt_license_ajax.i18n.messages.clear_cache + '</td>' );
        }
      })

      .done( function( response2, textStatus, jqXHR ) {
        var notice = 'success';
        var color  = '#46b450';
        $( '#wpt-row-notice-' + locale + '-' + slug ).html( '<td colspan="6" class="wpt-notice-table wpt-notice-table-' + notice + '">' + spinner( slug, locale, notice ) + response.data.message + '</td>' ).fadeOut(1000);
      });
    });
  });

  // Premium Activate License
  $( "[id^=activate-license-]" ).live( "click", function(e) {
    e.preventDefault();

    var slug    = $( this ).attr( 'data-slug' );
    var name    = $( this ).attr( 'data-name' );
    var locale  = $( this ).attr( 'data-locale' );
    var type    = $( this ).attr( 'data-type' );
    var license = $( '#wpt-license-' + locale + '-' + slug ).val();
    var notice  = 'info';
    var color   = '#00A0D2';

    $.ajax({
      type: "POST",
      url : wpt_license_ajax.ajaxurl,
      data: {
        'action'  : 'activateLicense',
        'nonce'   : wpt_license_ajax.nonce,
        'slug'    : slug,
        'type'    : type,
        'locale'  : locale,
        'license' : license,
      },
      beforeSend: function(response) {
        $( '#wpt-spinner-' + locale + '-' + slug ).addClass( 'is-active' );
        $( '#wpt-row-license-' + locale + '-' + slug + ' .column-ajax' ).addClass( 'is-' + notice );
        $( '#wpt-row-notice-' + locale + '-' + slug )
          .show()
          .html( '<td colspan="6" class="wpt-notice-table wpt-notice-table-' + notice + '">' + spinner( slug, locale, notice ) + wpt_license_ajax.i18n.messages.check_license + '</td>' );
      },
    })

    .done( function( response, textStatus, jqXHR ) {

      if ( 'valid' == response.data.api.license ) {

        $( '#wpt-spinner-' + locale + '-' + slug ).removeClass( 'is-active' ).addClass( 'is-success' );
        $( '#wpt-col-status-' + locale + '-' + slug ).html( '<p>' + wpt_license_ajax.i18n.status.activated + '</p>' );

      } else {

        var notice = 'error';
        var color = '#DC3232';

        if ( 'expired' == response.data.api.license || 'expired' == response.data.api.error ) {
          $( '#wpt-col-actions-' + locale + '-' + slug ).append(
            renew_link( slug, locale, license ) +
            activate_button( slug, name, locale, type )
          );
        }
        if ( 'no_activations_left' == response.data.api.error ) {

          var upgrade_id = parseInt( response.data.api.price_id ) + 1;

          if ( upgrade_id <= wpt_license_ajax.products[locale][slug].pricing ) {
            $( '#wpt-col-actions-' + locale + '-' + slug ).append( upgrade_link( slug, locale, license, response.data.api, upgrade_id ) );
          }

          $( '#wpt-col-actions-' + locale + '-' + slug ).append( activate_button( slug, name, locale, type ) );
        }

      }

      $( '#wpt-col-actions-' + locale + '-' + slug ).remove( '#activate-license-' + slug );
      $( '#wpt-row-notice-' + locale + '-' + slug ).html( '<td colspan="6" class="wpt-notice-table wpt-notice-table-' + notice + '">' + spinner( slug, locale, notice ) + response.data.message + '</td>' );
      if( 'error' == notice ) {
        return;
      }
    })
    .then( function( response, textStatus, jqXHR ) {
      if( false == response.success ) {
        return;
      }
      var notice = 'info';
      var color  = '#00A0D2';

      $.ajax({
        type: "POST",
        url: wpt_license_ajax.ajaxurl,
        data: {
          'action': 'clearCacheUpdate',
          'nonce'  : wpt_license_ajax.nonce,
          'type'   : type,
        },
        beforeSend: function(response2) {
            $( '#wpt-row-notice-' + locale + '-' + slug ).html( '<td colspan="6" class="wpt-notice-table wpt-notice-table-' + notice + '">' + spinner( slug, locale, notice ) + wpt_license_ajax.i18n.messages.check_update + '</td>' );
        }
      })

      .done( function( response2, textStatus, jqXHR ) {
          $( '#wpt-row-notice-' + locale + '-' + slug ).html( '<td colspan="6" class="wpt-notice-table wpt-notice-table-' + notice + '">' + spinner( slug, locale, notice ) + response2.data.message + '</td>' );
      })
      .then( function( response2, textStatus, jqXHR ) {
        if( false == response.success ) {
          return;
        }
        var notice = 'info';
        var color  = '#00A0D2';

        $.ajax({
          type: "POST",
          url: wpt_license_ajax.ajaxurl,
          data: {
            'action': 'upgradeTranslation',
            'nonce'  : wpt_license_ajax.update_nonce,
            'slug'   : slug,
            'type'   : type,
            'locale' : locale,
          },
          beforeSend: function(response3) {
              $( '#wpt-row-notice-' + locale + '-' + slug ).html( '<td colspan="6" class="wpt-notice-table wpt-notice-table-' + notice + '">' + spinner( slug, locale, notice ) + wpt_license_ajax.i18n.messages.download_lp + '</td>' );
          }
        })

        .done( function( response3, textStatus, jqXHR ) {
          var notice = 'success';
          var color  = '#46b450';
          $('#wpt-spinner-' + locale + '-' + slug ).removeClass( 'is-active' ).addClass( 'is-success' );
          $( '#wpt-row-license-' + locale + '-' + slug + ' .column-ajax' ).removeClass('is-info').addClass( 'is-' + notice );
          $( '#wpt-row-notice-' + locale + '-' + slug ).html( '<td colspan="6" class="wpt-notice-table wpt-notice-table-' + notice + '">' + spinner( slug, locale, notice ) + wpt_license_ajax.i18n.messages.lp_installed + '</td>' ).fadeOut(1000);
          $( '#wpt-col-actions-' + locale + '-' + slug ).html( deactivate_button( slug, name, locale, type ) + delete_button( slug, name, locale, type ) );
        });
      })
    });
  });

  // Premium Check License
  $( "[id^=check-license-]" ).live( "click", function(e) {
    e.preventDefault();

    var slug    = $( this ).attr( 'data-slug' );
    var name    = $( this ).attr( 'data-name' );
    var type    = $( this ).attr( 'data-type' );
    var locale  = $( this ).attr( 'data-locale' );
    var license = $( '#wpt-license-' + locale + '-' + slug ).val();

    $.ajax({
      type: "POST",
      url : wpt_license_ajax.ajaxurl,
      data: {
        'action'  : 'checkLicense',
        'nonce'   : wpt_license_ajax.nonce,
        'slug'    : slug,
        'type'    : type,
        'name'    : name,
        'locale'  : locale,
      },
      beforeSend: function(response) {
        $('#wpt-spinner-' + locale + '-' + slug ).addClass( 'is-active' );
      },
    })

    .done( function( response, textStatus, jqXHR ) {
      $( '#wpt-spinner-' + locale + '-' + slug ).removeClass( 'is-active' );
      $( '#wpt-row-notice-' + locale + '-' + slug ).slideDown( 'slow' );
      $( '#wpt-col-actions-' + locale + '-' + slug ).html( spinner( slug, locale, notice ) );

      if ( 'valid' == response.data.api.license && 0 != response.data.api.activations_left ) {
        var notice = 'success';
        $( '#wpt-col-actions-' + locale + '-' + slug ).append( activate_button( slug, name, locale, type ) );
        $( 'check-license-' + slug ).hide();
        $( 'wpt-renew-' + locale + '-' + slug ).hide();
        $( 'wpt-upgrade-' + locale + '-' + slug ).hide();
      } else {
        var notice = 'error';
        if( 'expired' == response.data.api.license || 'expired' == response.data.api.error ) {
          $( '#wpt-col-actions-' + locale + '-' + slug ).append(
            renew_link( slug, locale, license ) +
            check_button( slug, name, locale, type )
          );
        }
        if ( 0 < response.data.api.activations_left ) {
          $( '#wpt-col-actions-' + locale + '-' + slug ).append(
            upgrade_link( slug, locale, license, response.data.api ) +
            check_button( slug, name, locale, type )
          );
        }
      }

      $( '#wpt-row-notice-' + locale + '-' + slug + ' td' ).html( '<td colspan="6" class="wpt-notice-table wpt-notice-table-' + notice + '">' + response.data.message + '</td>' ).fadeOut(1000);
      $( '#wpt-col-actions-' + locale + '-' + slug ).append( delete_button( slug, name, locale, type ) );

    });
  });

  // Premium Deactivate License
  $( "[id^=deactivate-license-]" ).live( "click", function(e) {
    e.preventDefault();

    var slug    = $( this ).attr( 'data-slug' );
    var name    = $( this ).attr( 'data-name' );
    var type    = $( this ).attr( 'data-type' );
    var locale  = $( this ).attr( 'data-locale' );
    var license = $( '#wpt-license-' + locale + '-' + slug ).val();
    var notice  = 'info';
    var color   = '#00A0D2';

    $.ajax({
      type: "POST",
      url : wpt_license_ajax.ajaxurl,
      data: {
        'action'  : 'deactivateLicense',
        'nonce'   : wpt_license_ajax.nonce,
        'slug'    : slug,
        'type'    : type,
        'locale'  : locale,
        'license' : license,
      },
      beforeSend: function(response) {
        $('#wpt-spinner-' + locale + '-' + slug ).addClass( 'is-active' );
        $( '#wpt-row-notice-' + locale + '-' + slug )
          .show()
          .html( '<td colspan="6" class="wpt-notice-table wpt-notice-table-' + notice + '">' + spinner( slug, locale, notice ) + wpt_license_ajax.i18n.messages.deactivating + '</td>' );
      },
    })

    .done( function( response, textStatus, jqXHR ) {

      if ( true === response.success ) {
        var notice = 'success';
        var color  = '#46b450';
      } else {
        var notice = 'error';
        var color = '#DC3232';
      }
      $( '#wpt-row-notice-' + locale + '-' + slug ).html( '<td colspan="6" class="wpt-notice-table wpt-notice-table-' + notice + '">' + spinner( slug, locale, notice ) + response.data.message + '</td>' ).fadeOut(1000);
      $( '#wpt-col-actions-' + locale + '-' + slug ).html(
        activate_button( slug, name, locale, type ) +
        delete_button( slug, name, locale, type )
      );
      $( '#wpt-col-status-' + locale + '-' + slug ).html('');
    });
  });

  /*
   * ----------------------
   * Feature : Translations
   * ----------------------
   */

  // Translations Update
  $( ".wpt-button-update" ).live( "click", function(e) {
    e.preventDefault();

    var slug    = $( this ).attr( 'data-slug' );
    var locale  = $( this ).attr( 'data-locale' );
    var type    = $( this ).attr( 'data-type' );
    var updates = $( '#wp-admin-bar-updates .ab-label').val();

    $.ajax({
      type: "POST",
      url: wpt_license_ajax.ajaxurl,
      data: {
        'action': 'upgradeTranslation',
        'nonce'   : wpt_license_ajax.update_nonce,
        'slug'   : slug,
        'type'   : type,
        'locale' : locale,
      },
      beforeSend: function(response) {
        $( '#wpt-row-notice-' + locale + '-' + slug )
          .show()
          .html( '<td colspan="5" class="wpt-notice-table wpt-notice-table-' + notice + '">' + spinner( slug, locale, notice ) + wpt_license_ajax.i18n.messages.download_lp + '</td>' );
      }
    })
    .done( function( response, textStatus, jqXHR ) {
      var notice = 'success';
      var color  = '#46b450';
      $( '#wpt-table-row-' + locale + '-' + slug + ' .wpt-button-update' ).fadeOut(1000);
      $( '#wpt-row-notice-' + locale + '-' + slug ).html( '<td colspan="5" class="wpt-notice-table wpt-notice-table-' + notice + '">' + spinner( slug, locale, notice ) + wpt_license_ajax.i18n.messages.lp_installed + '</td>' ).fadeOut(1000);

    });

  });

  // Translation Get Settings
  $( ".wp-button-translations-settings" ).live( "click", function(e) {
    e.preventDefault();

    var slug   = $( this ).attr( 'data-slug' );
    var locale = $( this ).attr( 'data-locale' );

    $.ajax({
      type: "POST",
      url : wpt_license_ajax.ajaxurl,
      data: {
        'action'   : 'getTranslationSettings',
        'nonce'    : wpt_license_ajax.translation_nonce,
        'slug'     : slug,
        'locale'   : locale,
      },
      beforeSend: function(response) {
        $( '#wpt-settings-overlay-' + slug + '-' + locale ).show();
      }
    })
    .done( function( response, textStatus, jqXHR ) {
      if ( false != response.data.settings ) {
        if( '1' == response.data.settings.lock_translation ) {
          $( '#translation-lock-translation-' + slug + '-' + locale ).prop( "checked", true );
        } else {
          $( '#ranslation-lock-translation-' + slug + '-' + locale ).prop( "checked", false );
        }
        if( '1' == response.data.settings.use_custom ) {
          $( '#translation-use_custom-' + slug + '-' + locale ).prop( "checked", true );
          $( '#wpt-field-row-translation-custom_translation-' + slug + '-' + locale ).removeClass( 'hidden' );
          if ( typeof response.data.settings.custom_translation !== "undefined" && response.data.settings.custom_translation.path !== null ) {
            $( '#wpt-field-row-translation-custom_translation-' + slug + '-' + locale + ' .column-actions' ).html( '<span>' + response.data.settings.custom_translation.filename + '</span> <button class="wpt-delete-custom-file wpt-button danger" data-slug="' + slug + '" data-locale="' + locale + '"><span class="dashicons dashicons-trash"></span> <span class="screen-reader-text">' + wpt_license_ajax.i18n.translation.delete_file + '</span></button>' );
          }
        } else {
          $( '#translation-use_custom-' + slug + '-' + locale ).prop( "checked", false );
        }
        $( '#translation-repo-priority-' + slug + '-' + locale ).removeAttr('selected');
        $( '#translation-repo-priority-' + slug + '-' + locale + ' option[value=' + response.data.settings.repo_priority + ']').prop("selected", "selected")
      }
      $( '#wpt-settings-overlay-' + slug + '-' + locale ).fadeOut(500);
    });

  });

  // Translation Edit
  $( ".wpt-translations-settings-submit" ).live( "click", function(e) {
    e.preventDefault();

    var notice  = 'info';
    var color   = '#00A0D2';

    var slug   = $( this ).attr( 'data-slug' );
    var locale = $( this ).attr( 'data-locale' );
    var repo_priority = $('#translation-repo-priority-' + slug + '-' + locale ).val();
    var lock_translation = $('#translation-lock-translation-' + slug + '-' + locale ).is(':checked') ? 1 : 0;
    var use_custom       = $('#translation-use_custom-' + slug + '-' + locale ).is(':checked') ? 1 : 0;
    var form_data = new FormData();

    if ( $( '#translation-custom_translation-' + slug + '-' + locale ).length ) {
      var file_data = $( '#translation-custom_translation-' + slug + '-' + locale ).prop('files')[0];
      form_data.append( 'file',     file_data );
    }

    form_data.append( 'action',   'saveTranslationSettings' );
    form_data.append( 'nonce',    wpt_license_ajax.translation_nonce );
    form_data.append( 'slug',     slug );
    form_data.append( 'locale',   locale );
    form_data.append( 'repo_priority', repo_priority );
    form_data.append( 'lock_translation', lock_translation );
    form_data.append( 'use_custom', use_custom );

    $.ajax({
      type: "POST",
      url : wpt_license_ajax.ajaxurl,
      processData: false, // Don't process the files
      contentType: false,
      cache: false,
      data: form_data,
      beforeSend: function(response) {
        $( '#wpt-modal-settings-notice-' + slug + '-' + locale )
          .show()
          .html( '<td colspan="2" class="wpt-notice-table wpt-notice-table-' + notice + '">' + wpt_license_ajax.i18n.translation.saving + '<span class="wpt-spinner alignright"></span></td>' );
      }
    })
    .done( function( response, textStatus, jqXHR ) {
      if ( true === response.success ) {
        var notice = 'success';
        var color  = '#46b450';
        var repo = ( 'all' != response.data.repo ) ? response.data.repo : wpt_license_ajax.i18n.translation.all;
        if ( 1 == lock_translation ) {
          $( '#wpt-license-row-' + locale + '-' + slug + ' .wpt-button-update' ).hide();
          $( '#wpt-license-row-' + locale + '-' + slug + ' .wpt-lock').html( '<span class="dashicons dashicons-lock"></span>' );
        } else {
          $( '#wpt-license-row-' + locale + '-' + slug + ' .dashicons-lock').fadeOut();
        }
        $( '#wpt-modal-settings-notice-' + slug + '-' + locale ).html( '<td colspan="2" class="wpt-notice-table wpt-notice-table-' + notice + '">' + response.data.message + repo_spinner( slug, 'success' ) + '</td>' ).slideUp();
        $( '#wpt-license-row-' + locale + '-' + slug + ' .wpt-repo-cell' ).html( repo );
        $( '#js-modal-close' ).click();
      } else {
        var notice = 'error';
        var color = '#DC3232';
        $.each( response.data.errors, function( index, value ) {
          $( '#wpt-field-row-' + value.field + '-' + slug + '-' + locale ).addClass( 'error' );
          $( '#wpt-field-notice-' + value.field + '-' + slug + '-' + locale  )
          .show()
          .html(  value.message );
        });
        $( '#wpt-modal-settings-notice-' + slug + '-' + locale ).html( '<td colspan="2" class="wpt-notice-table wpt-notice-table-' + notice + '">' + response.data.message + repo_spinner( slug, 'success' ) + '</td>' );
      }
    });

  });

  // Translation CustomFile Toggle
  $( '[id^=translation-use_custom-]' ).live( 'change', function() {

    var slug = $( this ).attr( 'data-slug' );
    var locale = $( this ).attr( 'data-locale' );

    if ( $("#translation-use_custom-" + slug + "-" + locale  ).is(':checked') ) {
      $( '#wpt-field-row-translation-custom_translation-' + slug + '-' + locale ).removeClass('hidden');
    } else {
      $( '#wpt-field-row-translation-custom_translation-' + slug + '-' + locale ).addClass('hidden');
    }
  });

  // CustomFile Delete
  $( ".wpt-delete-custom-file" ).live( "click", function(e) {
    e.preventDefault();

    var notice  = 'info';
    var color   = '#00A0D2';

    var slug   = $( this ).attr( 'data-slug' );
    var locale = $( this ).attr( 'data-locale' );

    $.ajax({
      type: "POST",
      url : wpt_license_ajax.ajaxurl,
      data: {
        'action'   : 'deleteCustomFile',
        'nonce'    : wpt_license_ajax.translation_nonce,
        'slug'     : slug,
        'locale'   : locale,
      },
      beforeSend: function(response) {
        $( '#wpt-modal-settings-notice-' + slug + '-' + locale )
          .show()
          .html( '<td colspan="2" class="wpt-notice-table wpt-notice-table-' + notice + '">' + wpt_license_ajax.i18n.translation.deleting_file + '<span class="wpt-spinner alignright"></span></td>' );
      }
    })
    .done( function( response, textStatus, jqXHR ) {
      var notice = 'success';
      var color  = '#46b450';
      $( '#wpt-modal-settings-notice-' + slug + '-' + locale ).html( '<td colspan="2" class="wpt-notice-table wpt-notice-table-' + notice + '">' + wpt_license_ajax.i18n.translation.deleted_file + repo_spinner( slug, 'success' ) + '</td>' ).slideUp();
      $( '#wpt-field-row-translation-custom_translation-' + slug + '-' + locale + ' .column-actions' ).html( response.data.html );
    });

  });


  /*
   * ----------------------
   * Feature : Repositories
   * ----------------------
   */

  // Repo Spinner
  var repo_spinner = function ( slug, type ) {
    html = '<span id="wpt-spinner-repo-'+ slug + '" class="wpt-spinner is-' + type + ' alignright"></span>';
    return html;
  }

  //Repo Button Delete
  var button_repo_delete = function ( slug ) {
    html = '<button id="delete-repo-' + slug + '" data-slug="' + slug + '" class="wpt-button danger wpt-delete-repository"><span class="dashicons dashicons-trash"></span> <span class="screen-reader-text">' + wpt_license_ajax.i18n.repo.delete + '</span></button>';
    return html;
  }

  //Repo Button Activate
  var button_repo_activate = function ( slug ) {
    html = '<button id="activate-repo-' + slug + '" class="wpt-button" data-slug="' + slug + '"><span class="dashicons dashicons-controls-play"></span> ' + wpt_license_ajax.i18n.btn.activate + '</button>';
    return html;
  }

  //Repo Button Deactivate
  var button_repo_deactivate = function ( slug ) {
    html = '<button id="deactivate-repo-' + slug + '" class="wpt-button" data-slug="' + slug + '"><span class="dashicons dashicons-controls-pause"></span> ' + wpt_license_ajax.i18n.btn.deactivate + '</button>';
    return html;
  }

  //Repo Button Edit
  var button_repo_edit = function ( slug ) {
    html = '<button id="label_modal_1" class="js-modal wpt-button wpt-button-repo-settings" data-slug="' + slug + '" data-modal-content-id="wpt-repo-modal" data-modal-prefix-class="wpt-modal" data-modal-close-text="<span>' + wpt_license_ajax.i18n.repo.close_modal + '</span>" data-modal-close-title="' + wpt_license_ajax.i18n.repo.close_modal + '" aria-haspopup="dialog"><span class="dashicons dashicons-admin-tools"></span> <span class="wpt-hide-on-lg">' + wpt_license_ajax.i18n.repo.edit + '</span></button>';
    return html;
  }

  // Repo Add
  $( ".wpt-repo-submit" ).live( "click", function(e) {
    e.preventDefault();

    var slug     = $( this ).attr( 'data-slug' );
    var form_action   = $( this ).attr( 'data-action' );

    $.ajax({
      type: "POST",
      url : wpt_license_ajax.ajaxurl,
      data: {
        'action':      'saveRepository',
        'nonce':       wpt_license_ajax.repository_nonce,
        'slug':        slug,
        'form_action': form_action,
        'name':        $( '#wpt-repo-form #repo-name' ).val(),
        'provider':    $( '#wpt-repo-form #repo-provider').val(),
        'type':        $( '#wpt-repo-form #repo-type' ).val(),
        'token':       $( '#wpt-repo-form #repo-token' ).val(),
        'url':         $( '#wpt-repo-form #repo-url' ).val(),
        'logo':        $( '#wpt-repo-form #repo-logo' ).val(),
        'translated':  $( '#wpt-repo-form #repo-translated' ).val(),
        'description': $( '#wpt-repo-form #repo-description' ).val(),
        'active':      $( '#wpt-repo-form #repo-active' ).is(':checked') ? 1 : '',
      },
      beforeSend: function(response) {
        $( '#wpt-modal-repo-notice' )
          .show()
          .html( '<td colspan="2" class="wpt-notice-table wpt-notice-table-' + notice + '">' + wpt_license_ajax.i18n.translation.saving + '<span class="wpt-spinner alignright"></span></td>' );
      }
    })
    .done( function( response, textStatus, jqXHR ) {

      if ( true === response.success ) {
        var notice = 'success';
        var color  = '#46b450';
      }
      if ( false=== response.success ) {
        var notice = 'error';
        var color = '#DC3232';
        $.each( response.data.errors, function( index, value ) {
          $( '#wpt-field-row-' + value.field ).addClass( 'error' );
          $( '#wpt-field-notice-' + value.field )
          .show()
          .html(  value.message );
        });
      }
      $( '#wpt-modal-repo-notice' ).html( '<td colspan="2" class="wpt-notice-table wpt-notice-table-' + notice + '">' + response.data.message + repo_spinner( name, notice ) + '</td>' );

      if ( 'success' == notice ) {
        $logo   = ( '' != response.data.repo.logo ) ? response.data.repo.logo : '<label>' + response.data.repo.name + '</label>';
        $type   = ( 'public' == response.data.repo.type ) ? '<span class="dashicons dashicons-admin-site" title="' + response.data.repo.type + '"></span>' : '<span class="dashicons dashicons-lock" title="' + response.data.repo.type + '"></span>';
        $button = ( '1' == response.data.repo.active ) ? button_repo_deactivate( response.data.slug ) : button_repo_activate( response.data.slug );
        if ( 'edit' != form_action ) {
          $( '#repositories-rows' ).append( '<tr id="wpt-table-row-repositories-' + response.data.slug + '" class="wpt-license-row"><td>' + $logo + '</td><td>' + response.data.repo.description + '</td><td><span class="wpt-repo-status">' + response.data.repo.provider + $type + ' <span class="screen-reader-text">' + response.data.repo.type + '</span></span></td><td class="column-actions">' + $button + ' ' + button_repo_delete( response.data.slug ) + '</td></tr>');
          $( '#repositories-rows' ).append( '<tr id="wpt-row-notice-repositories-' + response.data.slug + '" class="wpt-license-notice hidden"></tr>' );
        } else {
          $( '#wpt-table-row-repositories-' + response.data.slug ).html( '<td>' + $logo + '</td><td>' + response.data.repo.description + '</td><td><span class="wpt-repo-status">' + response.data.repo.provider + $type + ' <span class="screen-reader-text">' + response.data.repo.type + '</span></span></td><td class="column-actions">' + $button + ' ' + button_repo_edit( response.data.slug ) + ' ' + button_repo_delete( response.data.slug ) + '</td>');
        }
        $( '#js-modal-close' ).click();
      }

    });

  });

  // Repo getSettings
  $( ".wpt-button-repo-settings" ).live( "click", function(e) {
    e.preventDefault();

    $( '.wpt-notice-table' ).addClass('hidden');

    var slug          = $( this ).attr( 'data-slug' );
    var form_action   = $( this ).attr( 'data-action' );
    var title         = ( 'edit' == form_action ) ? wpt_license_ajax.i18n.repo.edit_title : wpt_license_ajax.i18n.repo.add_title;

    $( '#wpt-repo-form h2' ).html( title );

    if ( 'edit' == form_action ) {
      $.ajax({
        type: "POST",
        url : wpt_license_ajax.ajaxurl,
        data: {
          'action'   : 'getRepoSettings',
          'nonce'    : wpt_license_ajax.repository_nonce,
          'slug'     : slug,
        },
        beforeSend: function(response) {
          $( '#wpt-settings-overlay' ).show();
        }
      })
      .done( function( response, textStatus, jqXHR ) {
        if ( false != response.data.settings ) {
          $( '#wpt-repo-form #repo-name' ).val( response.data.settings.name );
          $( '#wpt-repo-form #repo-provider' ).removeAttr('selected');
          $( '#wpt-repo-form #repo-type' ).removeAttr('selected');
          $( '#wpt-repo-form #repo-provider option[value=' + response.data.settings.provider + ']').prop("selected", "selected")
          $( '#wpt-repo-form #repo-type option[value=' + response.data.settings.type + ']').prop("selected", "selected")
          $( '#wpt-repo-form #repo-url' ).val( response.data.settings.url );
          $( '#wpt-repo-form #repo-token' ).val( response.data.settings.token );
          $( '#wpt-repo-form #repo-translated' ).val( response.data.settings.translated );
          $( '#wpt-repo-form #repo-description' ).val( response.data.settings.description );

          if( 'glotpress' == response.data.settings.provider ) {
            $( '#wpt-field-row-repo-excludes' ).removeClass('hidden');
            $( '#wpt-field-row-repo-translated' ).removeClass('hidden');
          }

          if ( '1' == response.data.settings.active ) {
            $( '#wpt-repo-form #repo-active' ).prop( "checked", true );
          } else {
            $( '#wpt-repo-form #repo-active' ).prop( "checked", false );
          }
          $( '#repo-logo' ).val(  response.data.settings.logo );
          $( '#wpt-logo-preview' ).html( response.data.logo );
        }


        $( '#wpt-repo-form .wpt-repo-submit' ).attr( 'data-action', form_action );
        $( '#wpt-repo-form .wpt-repo-submit' ).attr( 'data-slug', slug );
        $( '#wpt-settings-overlay' ).fadeOut(500);
      });
    }

  });

  // Repo Delete
  $( ".wpt-delete-repository" ).live( "click", function(e) {
    e.preventDefault();

    var slug   = $( this ).attr( 'data-slug' );
    var notice  = 'info';
    var color   = '#00A0D2';

    $.ajax({
      type: "POST",
      url : wpt_license_ajax.ajaxurl,
      data: {
        'action'  : 'deleteRepository',
        'nonce'   : wpt_license_ajax.repository_nonce,
        'slug'    : slug,
      },
      beforeSend: function(response) {
        $( '#wpt-row-notice-repositories-' + slug )
          .show()
          .html( '<td colspan="4" class="wpt-notice-table wpt-notice-table-' + notice + '">'  + wpt_license_ajax.i18n.repo.deleting + '<span class="wpt-spinner alignright"></span></td>' );
      }
    })
    .done( function( response, textStatus, jqXHR ) {
      var notice = 'success';
      var color  = '#46b450';
      $( '#wpt-row-notice-repositories-' + slug ).html( '<td colspan="4" class="wpt-notice-table wpt-notice-table-' + notice + '">' + response.data.message+ repo_spinner( slug, 'success' ) + '</td>' );
      $( '#wpt-table-row-repositories-' + slug ).fadeOut(1000);
      $( '#wpt-row-notice-repositories-' + slug ).fadeOut(1000);
    });

  });

  //Repo Activate
  $( "[id^=activate-repo-]" ).live( "click", function(e) {
    e.preventDefault();

    var slug    = $( this ).attr( 'data-slug' );
    var notice  = 'info';
    var color   = '#00A0D2';

    $.ajax({
      type: "POST",
      url : wpt_license_ajax.ajaxurl,
      data: {
        'action'  : 'activateRepository',
        'nonce'   : wpt_license_ajax.repository_nonce,
        'slug'    : slug,
      },
      beforeSend: function(response) {
        $( '#wpt-row-notice-repositories-' + slug )
          .show()
          .html( '<td colspan="4" class="wpt-notice-table wpt-notice-table-' + notice + '">'  + wpt_license_ajax.i18n.repo.activating + '<span class="wpt-spinner alignright"></span> </td>' );
      }
    })
    .done( function( response, textStatus, jqXHR ) {
      var notice = 'success';
      var color  = '#46b450';
      $( '#wpt-row-notice-repositories-' + slug ).html( '<td colspan="4" class="wpt-notice-table wpt-notice-table-' + notice + '">' + response.data.message + repo_spinner( slug, 'success' ) + '</td>' ).fadeOut(1000);
      $( '#wpt-table-row-repositories-' + slug + ' > .column-actions' ).prepend( button_repo_deactivate( slug ) );
      $( '#wpt-table-row-repositories-' + slug + ' #activate-repo-' + slug ).remove();
    });

  });

  // Repo Deactivate
  $( "[id^=deactivate-repo-]" ).live( "click", function(e) {
    e.preventDefault();

    var slug    = $( this ).attr( 'data-slug' );
    var notice  = 'info';
    var color   = '#00A0D2';

    $.ajax({
      type: "POST",
      url : wpt_license_ajax.ajaxurl,
      data: {
        'action'  : 'deactivateRepository',
        'nonce'   : wpt_license_ajax.repository_nonce,
        'slug'    : slug,
      },
      beforeSend: function(response) {
        $( '#wpt-row-notice-repositories-' + slug )
          .show()
          .html( '<td colspan="4" class="wpt-notice-table wpt-notice-table-' + notice + '">'  + wpt_license_ajax.i18n.repo.deactivating + '<span class="wpt-spinner alignright"></span></td>' );
      }
    })
    .done( function( response, textStatus, jqXHR ) {
      var notice = 'success';
      var color  = '#46b450';
      $( '#wpt-row-notice-repositories-' + slug ).html( '<td colspan="4" class="wpt-notice-table wpt-notice-table-' + notice + '">' + response.data.message + repo_spinner( slug, 'success' ) + '</td>' ).fadeOut(1000);
      $( '#wpt-table-row-repositories-' + slug + ' > .column-actions' ).prepend( button_repo_activate( slug ) );
      $( '#wpt-table-row-repositories-' + slug + ' #deactivate-repo-' + slug ).remove();
    });

  });

  // Repo Provider Toggle
  $( '[id^=repo-provider-]' ).live( 'change', function() {
    var provider = $( this ).val();
    var repo     = $( this ).attr( 'data-slug' );

    if ( 'glotpress' == provider ) {
      $( '#wpt-field-row-repo-excludes-' + repo ).removeClass('hidden');
      $( '#wpt-field-row-repo-translated-' + repo ).removeClass('hidden');
    } else {
      $( '#wpt-field-row-repo-excludes-' + repo ).addClass('hidden');
      $( '#wpt-field-row-repo-translated-' + repo ).addClass('hidden');
    }

    if ( 'http' == provider || 'glotpress' == provider ) {
      $( '#wpt-field-row-repo-type-' + repo ).addClass('hidden');
    } else {
      $( '#wpt-field-row-repo-type-' + repo ).removeClass('hidden');
    }
  });

  // Repo Type Toggle
  $( '[id^=repo-type-]' ).live( 'change', function() {

    var repo = $( this ).attr( 'data-slug' );
    var type = $( this ).val();

    if ( 'private' == type ) {
      $( '#wpt-field-row-repo-token-' + repo ).removeClass( 'hidden' );
    } else {
      $( '#wpt-field-row-repo-token-' + repo ).addClass( 'hidden' );
    }
  });

  // Repo Media Modal
  $( 'input.wpt-repo-media-select' ).live( 'click', function(e) {
     e.preventDefault();
     var image_frame;
     var slug = $( this ).attr( 'data-slug' );
     if ( image_frame ) {
         image_frame.open();
     }
     // Define image_frame as wp.media object
     image_frame = wp.media({
       title: 'Select Media',
       multiple : false,
       library : {
            type : 'image',
        }
      });

     image_frame.on('close',function() {
      // On close, get selections and save to the hidden input
      // plus other AJAX stuff to refresh the image preview
      var selection =  image_frame.state().get('selection');
      var gallery_ids = new Array();
      var my_index = 0;
      selection.each(function(attachment) {
         gallery_ids[my_index] = attachment['id'];
         my_index++;
      });
      var ids = gallery_ids.join(",");
      $('input#repo-logo').val(ids);
      Refresh_Image(slug, ids);
     });

    image_frame.on('open',function() {
      // On open, get the id from the hidden input
      // and select the appropiate images in the media manager
      var selection =  image_frame.state().get('selection');
      ids = $('input#repo-logo').val().split(',');
      ids.forEach(function(id) {
        attachment = wp.media.attachment(id);
        attachment.fetch();
        selection.add( attachment ? [ attachment ] : [] );
      });

    });

    image_frame.open();
   });

  // Repo Refresh Logo
  var Refresh_Image = function ( slug, the_id ) {
    $.ajax({
      type: "POST",
      url: wpt_license_ajax.ajaxurl,
      data: {
        action: 'getRepoImage',
        id: the_id
      },
    })
    .done( function( response, textStatus, jqXHR ) {
      if ( response.success === true ) {
        $( '#wpt-logo-preview').html( response.data.image );
      }
    });
  }

  // Repo GlotPress Explorer
  $( ".wpt-repo-explorer-open" ).live( "click", function(e) {
    e.preventDefault();

    var repo  = $( this ).attr( 'data-repo' );

    $.ajax({
      type: "POST",
      url : wpt_license_ajax.ajaxurl,
      data: {
        'action'  : 'getRepoTree',
        'nonce'   : wpt_license_ajax.repository_nonce,
        'repo'    : repo,
      },
      beforeSend: function(response) {
        $(".wpt-repo-explorer-open").addClass('active wpt-button-primary');
      }
    })
    .done( function( response, textStatus, jqXHR ) {
      $(".wpt-repo-explorer-open").removeClass('active wpt-button-primary');
      $( '#wpt-field-row-repo-explorer-' + repo ).show();
      $( '#wpt-field-row-repo-explorer-' + repo + ' td' ).html( response );
    });
  });

  // Repo GlotPress Explore Branch
  $( ".wpt-repo-deploy-branch" ).live( "click", function(e) {
    e.preventDefault();

    var repo    = $( this ).attr( 'data-repo' );
    var project = $( this ).attr( 'data-project' );
    var path    = $( this ).attr( 'data-path' );
    var id      = $( this ).attr( 'data-id' );

    $.ajax({
      type: "POST",
      url : wpt_license_ajax.ajaxurl,
      data: {
        'action'  : 'getRepoTree',
        'nonce'   : wpt_license_ajax.repository_nonce,
        'repo'    : repo,
        'project' : project,
        'path'    : path,
      },
      beforeSend: function(response) {
        $('.wpt-repo-deploy-branch[data-id="' + id + '"]').addClass('active wpt-button-primary');
      }
    })
    .done( function( response, textStatus, jqXHR ) {
      $('.wpt-repo-deploy-branch[data-id="' + id + '"]').removeClass('active wpt-button-primary');
      $( '#wpt-repo-project-sublist-' + id ).show().html( response );
    });
  });

  /*
   * ----------------------
   * Feature : Languages
   * ----------------------
   */

  // Language Add
  $( "#wpt-install-language" ).live( "click", function(e) {
    e.preventDefault();

    var locale  = $( "#wpt-dropdown-languages" ).val();
    var tab     = 'locales';
    var colspan = $( this ).attr( 'data-colspan' );

    $.ajax({
      type: "POST",
      url : wpt_license_ajax.ajaxurl,
      data: {
        'action'  : 'addLocale',
        'nonce'   : wpt_license_ajax.translation_nonce,
        'locale'  : locale,
        'tab'     : tab,
        'colspan' : colspan
      },
      beforeSend: function(response) {
        var notice  = 'info';
        var color   = '#00A0D2';
        $( '#languages-rows' ).append( '<tr id="wpt-table-row-' + tab + '-' + locale + '" class="wpt-license-row"><td colspan="' + colspan + '">' + locale + '</td></tr>' );
        $( '#languages-rows' ).append( '<tr id="wpt-row-notice-' + tab + '-' + locale + '" class="wpt-license-notice"><td colspan="' + colspan + '" class="wpt-notice-table wpt-notice-table-' + notice + '">' + wpt_license_ajax.i18n.locale.installing + '</td></tr>' );
      }
    })
    .done( function( response, textStatus, jqXHR ) {
      if ( true === response.success ) {
        var notice = 'success';
        var color  = '#46b450';

      } else {
        var notice = 'error';
        var color = '#DC3232';
      }
      $( '#wpt-row-notice-' + tab + '-' + locale + ' td' ).html( response.message );
      $( '#wpt-table-row-' + tab + '-' + locale ).html( response.data.html );
      $( '#wpt-row-notice-' + tab + '-' + locale ).fadeOut(1000);
    });
  });

  // Language Delete
  $( ".wpt-delete-language" ).live( "click", function(e) {
    e.preventDefault();

    var locale  = $( this ).attr( 'data-locale' );
    var tab     = $( this ).attr( 'data-tab' );
    var colspan = $( this ).attr( 'data-colspan' );

    $.ajax({
      type: "POST",
      url : wpt_license_ajax.ajaxurl,
      data: {
        'action'  : 'deleteLocale',
        'nonce'   : wpt_license_ajax.translation_nonce,
        'locale'  : locale,
        'tab'     : tab,
        'colspan' : colspan
      },
      beforeSend: function(response) {

        var notice  = 'info';
        var color   = '#00A0D2';
        $( '#wpt-row-notice-' + tab + '-' + locale )
        .show()
        .html( '<td colspan="' + colspan + '" class="wpt-notice-table wpt-notice-table-' + notice + '">' + spinner( tab, locale, colspan ) + wpt_license_ajax.i18n.locale.deleting + '</td>' );
      }
    })
    .done( function( response, textStatus, jqXHR ) {
      var notice = 'success';
      var color  = '#46b450';
      $( '#wpt-row-notice-' + tab + '-' + locale ).html( '<td colspan="' + colspan + '" class="wpt-notice-table wpt-notice-table-' + notice + '">' + wpt_license_ajax.i18n.locale.deleted + '</td>' );
      $( '#wpt-table-row-' + tab + '-' + locale ).fadeOut(1000);
      $( '#wpt-row-notice-' + tab + '-' + locale ).fadeOut(1000);
    });
  });


  /*
   * ----------------------
   * Feature : Capabilities
   * ----------------------
   */

  $( '#wpt-add-cap').live( "click", function(e) {
    e.preventDefault();
    $( '#capabilities-rows' ).append( '<tr class="wpt-license-row"><td><select>' + wpt_license_ajax.dropdown_roles + '</select></td><td><select>' + wpt_license_ajax.dropdown_caps + '</select></td><td class="column-actions"><button class="wpt-button">Save</button></td></tr>' );
  });

});
