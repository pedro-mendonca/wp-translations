// process.env.DISABLE_NOTIFIER = true; // Uncomment to disable all Gulp notifications.

/**
 * WP_Translations.
 *
 * This file adds gulp tasks to the WP_Translations Plugin.
 *
 * @author Sadler Jerome
 */

// Require our dependencies.
var args       = require('yargs').argv,
  autoprefixer = require('autoprefixer'),
  browsersync  = require('browser-sync'),
  bump         = require('gulp-bump'),
  changecase   = require('change-case'),
  del          = require('del'),
  mqpacker     = require('css-mqpacker'),
  fs           = require('fs'),
  gulp         = require('gulp'),
  beautify     = require('gulp-cssbeautify'),
  cache        = require('gulp-cached'),
  cleancss     = require('gulp-clean-css'),
  csscomb      = require('gulp-csscomb'),
  cssnano      = require('gulp-cssnano'),
  filter       = require('gulp-filter'),
  imagemin     = require('gulp-imagemin'),
  notify       = require('gulp-notify'),
  pixrem       = require('gulp-pixrem'),
  plumber      = require('gulp-plumber'),
  postcss      = require('gulp-postcss'),
  rename       = require('gulp-rename'),
  replace      = require('gulp-replace'),
  sass         = require('gulp-sass'),
  sort         = require('gulp-sort'),
  sourcemaps   = require('gulp-sourcemaps'),
  uglify       = require('gulp-uglify'),
  wpPot        = require('gulp-wp-pot'),
  zip          = require('gulp-zip'),
  jsonfile     = require('jsonfile');

// Set assets paths.
var paths = {
  all:     ['./**/*', '!./node_modules/', '!./node_modules/**', '!./screenshot.png', '!./assets/img/**'],
  images:  ['assets/img/*', '!assets/img/*.svg'],
  php: ['./*.php', './**/*.php', './**/**/*.php'],
  scripts: ['assets/js/*.js'],
  styles:  ['assets/scss/*.scss', '!assets/css']
};

/**
 * Compile Sass.
 *
 * https://www.npmjs.com/package/gulp-sass
 */
gulp.task('styles', function () {

  gulp.src('assets/scss/wpt-styles.scss')

    // Notify on error
    .pipe(plumber({
      errorHandler: notify.onError("Error: <%= error.message %>")
    }))

    // Source maps init
    .pipe(sourcemaps.init())

    // Process sass
    .pipe(sass({
      outputStyle: 'expanded'
    }))

    // Pixel fallbacks for rem units.
    .pipe(pixrem())

    // Parse with PostCSS plugins.
    .pipe(postcss([
      autoprefixer({
        browsers: 'last 2 versions'
      }),
      mqpacker({
        sort: true
      }),
    ]))

    // Format non-minified stylesheet.
    .pipe(csscomb())

    // Output non minified css.
    .pipe(gulp.dest('assets/css'))

    // Inject changes via browsersync.
    .pipe(browsersync.reload({
      stream: true
    }))

    // Process sass again.
    .pipe(sass({
      outputStyle: 'compressed'
    }))

    // Combine similar rules.
    .pipe(cleancss({
      level: {
        2: {
          all: true
        }
      }
    }))

    // Minify and optimize style.css again.
    .pipe(cssnano({
      safe: false,
      discardComments: {
        removeAll: true,
      },
    }))

    // Add .min suffix.
    .pipe(rename({
      suffix: '.min'
    }))

    // Write source map.
    .pipe(sourcemaps.write('./'))

    // Output the compiled sass to this directory.
    .pipe(gulp.dest('assets/css'))

    // Filtering stream to only css files.
    .pipe(filter('**/*.css'))

    // Notify on successful compile (uncomment for notifications).
    .pipe(notify("Compiled: <%= file.relative %>"));

});

/**
 * Minify javascript files.
 *
 * https://www.npmjs.com/package/gulp-uglify
 */
gulp.task('scripts', function () {

  return gulp.src(paths.scripts) // or other selection
   .pipe(uglify())
   .pipe(rename({suffix: '.min'})).pipe(gulp.dest('assets/js/min'))

  // Notify on successful compile.
  .pipe(notify("Minified: <%= file.relative %>"));

});

/**
 * Optimize images.
 *
 * https://www.npmjs.com/package/gulp-imagemin
 */
gulp.task('images', function () {

  return gulp.src(paths.images)

    // Notify on error.
    .pipe(plumber({
      errorHandler: notify.onError("Error: <%= error.message %>")
    }))

    // Cache files to avoid processing files that haven't changed.
    .pipe(cache('images'))

    // Optimize images.
    .pipe(imagemin({
      progressive: true
    }))

    // Output the optimized images to this directory.
    .pipe(gulp.dest('assets/img'))

    // Inject changes via browsersync.
    .pipe(browsersync.reload({
      stream: true
    }))

    // Notify on successful compile.
    .pipe(notify("Optimized: <%= file.relative %>"));

});

/**
 * Scan the theme and create a POT file.
 *
 * https://www.npmjs.com/package/gulp-wp-pot
 */
gulp.task('translate', function () {

  return gulp.src(paths.php)

    .pipe(plumber({
      errorHandler: notify.onError("Error: <%= error.message %>")
    }))

    .pipe(sort())

    .pipe(wpPot({
      domain: 'wp-translations',
      package: 'WP_Translations',
      bugReport: 'https://wp-translations.org',
      lastTranslator: 'FX Bénard <fx@wp-translations.pro>',
      team: 'WP-Translations <contact@wp-translations.pro>'
    }))

    .pipe(gulp.dest('./languages/wp-translations.pot'));

});

/**
 * Package Plugin.
 *
 * https://www.npmjs.com/package/gulp-zip
 */
gulp.task('build', function () {

    var file    = 'builds/builds.json';
    var output = {
      "id":          "wp-translations.org/plugins/wp-translations",
      "slug":        "wp-translations",
      "plugin":      "wp-translations/wp-translations.php",
      "new_version": args.to,
      "url":         "https://wp-translations.org",
      "tested":      args.tested,
      "last_updated": new Date(),
      "package":     "https://gitlab.com/wp-translations-repo/plugins/wp-translations/raw/master/builds/" + args.to + "/wp-translations.zip",
      "icons": {
        "1x":      "https://gitlab.com/wp-translations-repo/plugins/wp-translations/raw/master/builds/assets/icon-128x128.png",
        "default": "https://gitlab.com/wp-translations-repo/plugins/wp-translations/raw/master/builds/assets/icon-128x128.png",
      }
    };

    jsonfile.writeFileSync(file, output, {spaces: 2})
    gulp.src(['./**/*', '!./node_modules/', '!./node_modules/**', '!./assets/scss/', '!./assets/scss/**', '!./builds/', '!./builds/**', '!./CODE_OF_CONDUCT.md', '!./CONTRIBUTING.md', '!./package.json', '!./package-lock.json', '!./gulpfile.js', '!./composer.json'], {base: '../'})

    .pipe(zip(__dirname.split("/").pop() + '.zip'))
    .pipe(gulp.dest('./builds/' + args.to + '/'));

});

/**
 * Process tasks and reload browsers on file changes.
 *
 * https://www.npmjs.com/package/browser-sync
 *
 * If you are not using a self-signed certificate, use the below config:
 *
 * browsersync( {
 *	    proxy: 'wp-translations-org.dev',
 *	    notify: false,
 *	    open: false,
 * } );
 */
gulp.task('watch', function () {

  // HTTPS (optional).
  /*browsersync({
    proxy: 'http://wp-client.test',
    port: 8000,
    notify: false,
    open: false
  });*/

  // Run tasks when files change.
  gulp.watch(paths.styles, ['styles']);
  gulp.watch(paths.scripts, ['scripts']);
  gulp.watch(paths.images, ['images']);
  //gulp.watch(paths.php).on('change', browsersync.reload);

});

/**
 * Bump version.
 *
 * https://www.npmjs.com/package/gulp-bump
 */
gulp.task('bump', function () {

  if (args.major) {
    var kind = 'major';
  }

  if (args.minor) {
    var kind = 'minor';
  }

  if (args.patch) {
    var kind = 'patch';
  }

  gulp.src(['./wp-translations.php'])
  .pipe(bump({
    type: kind,
    version: args.to
  }))
  .pipe(bump({
    key: "'WPTORG_VERSION',",
    type: kind,
    version: args.to
  }))
  .pipe(bump({
    key: "Version:",
    type: kind,
    version: args.to
  }))
  .pipe(gulp.dest('./'));

  gulp.src(['./readme.txt'])
  .pipe(bump({
    key: "Tested up to:",
    type: kind,
    version: args.tested
  }))
  .pipe(bump({
    key: "Stable tag:",
    type: kind,
    version: args.to
  }))
  .pipe(gulp.dest('./'));
});


/**
 * Create default task.
 */
gulp.task('default', ['watch'], function () {

  gulp.start('styles', 'scripts', 'images');

});
