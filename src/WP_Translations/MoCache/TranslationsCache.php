<?php

namespace WP_Translations\MoCache;

defined( 'ABSPATH' ) or die( 'You don&#8217;t have permission to do this.' );

use WP_Translations\Models\HooksAdminInterface;
use WP_Translations\WordPress\Helpers\Helper;

/**
 * Use oject cache for mo files
 *
 * @since 1.0.0
 */

class EnqueueAssets implements HooksAdminInterface {

  public function __construct() {
    $this->options         = Helper::getOptions();
    $this->isActivate      = isset( $options['performance']['perf-mo-cache'] ) ? (bool) $options['performance']['perf-mo-cache'] : false;
    $this->isRegularPlugin = did_action( 'muplugins_loaded' );
    $this->isLoaded        = did_action( 'inpsyde_translation_cache' );
  }

  /**
   * @see WP_Translations\Models\HooksInterface
   */
  public function hooks() {

  }

}
