<?php

namespace WP_Translations\ThirdParty;

defined( 'ABSPATH' ) or die( 'You don&#8217;t have permission to do this.' );

use WP_Translations\Models\HooksAdminInterface;
use WP_Translations\WordPress\Helpers\TranslationHelper;

/**
 * Specfic methods for Elegant Themes plugins and themes
 *
 * @since 1.0.0
 */

class ElegantThemes implements HooksAdminInterface {

  /**
   * @see WP_Translations\Models\HooksInterface
   */
  public function hooks() {

    $theme = wp_get_theme();
    if ( 'Divi' === $theme->get( 'Name' ) ) {
      add_action('upgrader_process_complete', 'et_pb_force_regenerate_templates' );
    }

  }

}
