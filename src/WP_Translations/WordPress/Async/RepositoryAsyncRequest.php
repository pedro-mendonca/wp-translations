<?php
namespace WP_Translations\WordPress\Async;

defined( 'ABSPATH' ) or die( 'You don&#8217;t have permission to do this.' );

use WP_Translations\WordPress\Async\WPAsyncRequest;
use WP_Translations\WordPress\Helpers\RepositoryHelper;

class RepositoryAsyncRequest extends WPAsyncRequest {

  protected $action = 'wpt_repositories_async';

  /**
   * Task
   *
   * Override this method to perform any actions required on each
   * queue item. Return the modified item for further processing
   * in the next pass through. Or, return false to remove the
   * item from the queue.
   *
   * @param mixed $item Queue item to iterate over
   *
   * @return mixed
   */
  protected function handle() {

    RepositoryHelper::getRepoInfos( $_POST['slug'] );

    return false;
  }

}
