<?php

namespace WP_Translations\WordPress\Admin\Page;

defined( 'ABSPATH' ) or die( 'You don&#8217;t have permission to do this.' );

use WP_Translations\WordPress\Helpers\FeatureHelper;

abstract class PageLogs extends Page {

  public static function setPageActions() {

    $actions  = array();

    return apply_filters( WPTORG_SLUG . '_logs_page_actions', $actions );
  }

  public static function setTabs() {

    $tabs['logs']['update'] = array(
      'label' => __( 'Updates', 'wp-translations' ),
      'icon'  => 'dashicons-update',
      'order' => '0'
    );

    $tabs = apply_filters( WPTORG_SLUG . '_logs_tabs', $tabs );
    uasort( $tabs['logs'], function( $a, $b ) {
      return $a['order'] - $b['order'];
    });

    return $tabs;
  }

  public static function setFields() {

    $fields['logs'] = array();
    $tabs   = self::setTabs();
    $logs   = ( false !== get_site_option( 'wpt_logs' ) ) ? get_site_option( 'wpt_logs' ) : array();

    foreach( $tabs['logs'] as $tabKey => $tab ) {
      foreach ( $logs[ $tabKey ] as $key => $log ) {
        $fields['logs'][ $tabKey ][ $key ] = $log;
      }
    }

    return apply_filters( WPTORG_SLUG . '_logs_fields', $fields );
  }

  public static function setColumnsHeaders() {

    $columns = array();
    $tabs    = self::setTabs();

    $columns['logs']['update'] = array(
      'date' => array(
        'label' => __( 'Date', 'wp-translations' ),
        'class' => '',
        'order' => '0'
      ),
      'textdomain' => array(
        'label' => __( 'Text Domain', 'wp-translations' ),
        'class' => '',
        'order' => '20'
      ),
      'locale' => array(
        'label' => __( 'Language', 'wp-translations' ),
        'class' => '',
        'order' => '40'
      ),
      'version' => array(
        'label' => __( 'Version', 'wp-translations' ),
        'class' => '',
        'order' => '60'
      ),
      'action' => array(
        'label' => __( 'Action', 'wp-translations' ),
        'class' => '',
        'order' => '80'
      )
    );

    $columns = apply_filters( WPTORG_SLUG . '_logs_columns', $columns );

    foreach( $tabs['logs'] as $tabKey => $tab ) {
      uasort( $columns['logs'][ $tabKey ], function( $a, $b ) {
        return $a['order'] - $b['order'];
      });
    }

    return $columns;
  }

  public static function getColumnsCount( $tab ) {

    $tabs    = self::setTabs();
    $columns = self::setColumnsHeaders();
    $count   = array();

    foreach( array_keys( $tabs['logs'] ) as $tabKey ) {
      $count[ $tabKey ] = count( $columns['logs'][ $tabKey ] );
    }

    return $count[ $tab ];
  }

  public static function setRowAttribut( $tabKey, $fieldID, $field ) {

    $attributs = parent::setRowAttribut( $tabKey, $fieldID, $field );

    return $attributs;
  }

  public static function getColumn_date( $tabKey, $columnID, $fieldID, $field ) {

    $html  = '<td>';
    $html .= '<label>' . date_i18n( "Y-m-d H:i:s", $field['date'] ) . '</label>';
    $html .= '</td>';

    return $html;
  }

  public static function getColumn_license( $tabKey, $columnID, $fieldID, $field ) {

    $html  = '<td>' . $field['license'] . '</td>';

    return $html;
  }

  public static function getColumn_action( $tabKey, $columnID, $fieldID, $field ) {

    $class = ( 'update' == $tabKey ) ? 'column-actions' : '';
    $html  = '<td class="' . $class . '">' . $field['title'] . '</td>';

    return $html;
  }

  public static function getColumn_status( $tabKey, $columnID, $fieldID, $field ) {

    $class = ( 'update' != $tabKey ) ? 'column-actions' : '';
    $html  = '<td class="' . $class . '">' . $field['status']['message'] .' </td>';

    return $html;
  }

  public static function getColumn_textdomain( $tabKey, $columnID, $fieldID, $field ) {

    $textdomain = substr( $field['textdomain'], 0, strrpos( $field['textdomain'], '-' ) );

    $html  = '<td>' . $textdomain . '</td>';

    return $html;
  }

  public static function getColumn_locale( $tabKey, $columnID, $fieldID, $field ) {

    $language   = substr( strrchr( $field['textdomain'], "-" ), 1 );

    $html  = '<td>' . $language . '</td>';

    return $html;
  }

  public static function getColumn_version( $tabKey, $columnID, $fieldID, $field ) {

    $html  = '<td>' . $field['version']['product'] . ' rev(' . $field['version']['po_revision'] . ')</td>';

    return $html;
  }

  public static function setPageFooter() {

    $html = parent::setPageFooter();

    return apply_filters(  WPTORG_SLUG . '_logs_page_footer', $html );
  }

  public static function setPageDebug() {

    $logs = ( false !== get_site_option( 'wpt_logs' ) ) ? get_site_option( 'wpt_logs' ) : array();

    $debug = array(
      'repos' => array(
        'label' => __( 'Logs', 'wp-translations' ),
        'data'  => $logs
      )
    );

    return apply_filters(  WPTORG_SLUG . '_logs_page_debug', $debug );
  }

}
