<?php

namespace WP_Translations\WordPress\Admin\Page;

defined( 'ABSPATH' ) or die( 'You don&#8217;t have permission to do this.' );

use WP_Translations\WordPress\Helpers\Helper;
use WP_Translations\WordPress\Helpers\ModalHelper;
use WP_Translations\WordPress\Helpers\ProductHelper;
use WP_Translations\WordPress\Helpers\RepositoryHelper;
use WP_Translations\WordPress\Helpers\TranslationHelper;

abstract class PageTranslations extends Page {

  public static function setPageActions() {

    $actions       = array();
    $languages    = get_available_languages();
    $dropdownArgs = array(
      'id'                          => 'wpt-locale-switcher',
      'name'                        => 'wpt-locale-switcher',
      'languages'                   => get_available_languages(),
      'selected'                    => TranslationHelper::currentLocale(),
      'show_available_translations' => false,
      'show_option_site_default'    => false,
      'echo'                        => 0,
    );

    if ( $languages && 0 < count ( $languages ) ) {
      $actions['switcher']  = '<form id="wpt-settings-form" class="alignright" action="" method="POST">';
      $actions['switcher'] .= wp_dropdown_languages( $dropdownArgs );
      $actions['switcher'] .= '<input type="hidden" name="wpt-action" value="switchLanguage"/>';
      $actions['switcher'] .= '<input type="hidden" name="wpt-switch-locale-nonce" value="' . wp_create_nonce( 'wpt_switch_locale_nonce' ) . '"/>';
      $actions['switcher'] .= '<button type="submit" class="wpt-button"><span class="dashicons dashicons-randomize"></span> ' . esc_html__( 'Switch', 'wp-translations' ) . '</button>';
      $actions['switcher'] .= '</form>';
    } else {
      $actions['switcher'] = '<span>'. Helper::getLocale() .'</span>';
    }

    return apply_filters( WPTORG_SLUG . '_translations_page_actions', $actions );
  }

  public static function setTabs() {

    $types = TranslationHelper::allType();
    foreach( $types as $key => $type ) {
      $tabs['translations'][ $key ] = array(
        'label' => $type['label'],
        'icon'  => $type['icon'],
        'order' => '0'
      );
    }

    $tabs = apply_filters( WPTORG_SLUG . '_translations_tabs', $tabs );
    uasort( $tabs['translations'], function( $a, $b ) {
      return $a['order'] - $b['order'];
    });

    return $tabs;
  }

  public static function setFields() {

    $translations = TranslationHelper::getAllTranslations();

    foreach( $translations as $translation ) {
      $fields['translations'][ $translation->type ][ $translation->textdomain ] = array(
        'label' => $translation->name,
        'type'  => 'button',
        'data'  => (array) $translation,
        'order' => '0'
      );
    }

    return apply_filters( WPTORG_SLUG . '_translations_fields', $fields );
  }

  public static function setColumnsHeaders() {

    $tabs = self::setTabs();

    foreach ( $tabs['translations'] as $tabKey => $tab ) {

      $columns['translations'][ $tabKey ] = array(
        'option' => array(
          'label' => __( 'Name', 'wp-translations' ),
          'class' => '',
          'order' => '0'
        ),
        'textdomain' => array(
          'label' => __( 'Text Domain', 'wp-translations' ),
          'class' => '',
          'order' => '20'
        ),
        'loaded' => array(
          'label' => __( 'Loaded Translations', 'wp-translations' ),
          'class' => '',
          'order' => '40'
        ),
        'repositories' => array(
          'label' => __( 'Authorized Repositories', 'wp-translations' ),
          'class' => '',
          'order' => '60'
        ),
        'actions' => array(
          'label' => __( 'Actions', 'wp-translations' ),
          'class' => '',
          'order' => '99'
        )
      );

      $columns['translations'][ $tabKey ] = apply_filters( WPTORG_SLUG . '_settings_' . $tabKey . '_columns', $columns['translations'][ $tabKey ] );
      uasort( $columns['translations'][ $tabKey ], function( $a, $b ) {
        return $a['order'] - $b['order'];
      });

    }

    return $columns;
  }

  public static function setRowAttribut( $tabKey, $fieldID, $field ) {

    $attributs = array(
      'row_id'    => TranslationHelper::currentLocale() . '-' . $field['data']['textdomain'],
      'notice_id' => TranslationHelper::currentLocale() . '-' . $field['data']['textdomain'],
    );

    return $attributs;
  }

  public static function getColumnsCount( $tab ) {

    $tabs    = self::setTabs();
    $columns = self::setColumnsHeaders();
    $count   = array();

    foreach( array_keys( $tabs['translations'] ) as $tabKey ) {
      $count[ $tabKey ] = count( $columns['translations'][ $tabKey ] );
    }

    return $count[ $tab ];
  }

  public static function getColumn_option( $tabKey, $columnID, $fieldID, $field ) {

    $html  = '<td>';
    $html .= '<label>' . $field['data']['name'] . '</label>';
    $html .= '</td>';

    return $html;
  }

  public static function getColumn_textdomain( $tabKey, $columnID, $fieldID, $field ) {

    global $l10n;
    $l10n = ( is_array( $l10n ) ) ? $l10n : array();
    $loadedL10n = ( ! in_array( $field['data']['textdomain'], array_keys( $l10n ) ) ) ? '(' . esc_html_x( 'not loaded', 'text-domain', 'wp-translations' ) . ')' : '';

    $html  = '<td class="wpt-hide-on-lg">';
    $html .= $field['data']['textdomain'] . ' ' . $loadedL10n;
    $html .= '</td>';

    return $html;
  }

  public static function getColumn_loaded( $tabKey, $columnID, $fieldID, $field ) {

    $settings      = get_site_option( 'wpt_translations' );
    $currentLocale = TranslationHelper::currentLocale();
    $localRevision = TranslationHelper::getLocalPoRevisionDate( $field['data']['type'], $field['data']['textdomain'], $currentLocale );
    $version       = ( 0 !== $localRevision ) ? $field['data']['version'] . ' rev(' . $localRevision. ')' : esc_html__( 'No translations found', 'wp-translations' );
    $tooltip       = ( isset( $settings[ $field['data']['textdomain'] ][  $currentLocale ]['loaded_mo_file'] ) ) ? ' js-simple-tooltip" data-simpletooltip-content-id="tooltip-' . $field['data']['textdomain'] . '-' . $currentLocale : ' no-tooltip';
    $locked        = ( isset( $settings[ $field['data']['textdomain'] ][ $currentLocale ]['lock_translation'] ) &&
                      false !== (bool) $settings[ $field['data']['textdomain'] ][ $currentLocale ]['lock_translation'] ) ?
                       '<span class="dashicons dashicons-lock"></span>' : '';

    $html  = '<td class="wpt-hide-on-md">';
    $html .= '<span class="wpt-translation-version ' . $tooltip . '">' . $version . ' <span class="wpt-lock">' . $locked . '</span></span>';
    if ( ! empty( $tooltip ) ) {
      $html .= '<div id="tooltip-' . $field['data']['textdomain'] . '-' . $currentLocale . '" class="hidden">';
      $html .= 	TranslationHelper::getPoInfos( $field['data']['textdomain'], $currentLocale );
      $html .= '</div>';
    }
    $html .= '</td>';

    return $html;
  }

  public static function getColumn_repositories( $tabKey, $columnID, $fieldID, $field ) {

    $settings      = get_site_option( 'wpt_translations' );
    $currentLocale = TranslationHelper::currentLocale();
    $rewriteDomain = TranslationHelper::rewriteTextdomain( $field['data']['textdomain'], 'capitalize' );
    $repoSlug      = ( isset( $settings[ $field['data']['textdomain'] ][ $currentLocale ]['repo_priority'] ) ) ? $settings[ $field['data']['textdomain'] ][ $currentLocale ]['repo_priority'] : 'all';

    $html  = '<td class="wpt-repo-cell">';
    if( false !== ProductHelper::isProduct( $rewriteDomain, $currentLocale ) ) {
      $html .= RepositoryHelper::getRepoName( 'wpt-store' );
    } elseif ( 'all' == $repoSlug ) {
      $html .= esc_html__( 'All', 'wp-translations' );
    } elseif ( 'all' != $repoSlug ) {
      $html .= RepositoryHelper::getRepoName( $repoSlug );
    }
    $html .= '</td>';

    return $html;
  }

  public static function getColumn_actions( $tabKey, $columnID, $fieldID, $field ) {

    $settings            = get_site_option( 'wpt_translations' );
    $currentLocale       = TranslationHelper::currentLocale();
    $translationSettings = ( isset( $settings[ $field['data']['textdomain'] ][ $currentLocale ] ) ) ? $settings[ $field['data']['textdomain'] ][ $currentLocale ] : array();
    $args = array(
      'show_button' => false
    );

    $html = '<td class="column-actions">';
    foreach( $field['data']['updates'] as $update ) {
      if ( $currentLocale == ( $update->language ) ) {
      $html .= '<button class="wpt-button wpt-button-update" data-type="' . esc_attr( $update->type ) . '" data-locale="' . esc_attr( $update->language ) . '" data-slug="' . esc_attr( $update->slug ) . '" title="' . esc_html__( 'Update', 'wp-translations' ) . '"><span class="dashicons dashicons-update"></span> <span class="wpt-hide-on-lg">' . esc_html__( 'Update', 'wp-translations' ) . '</span></button>';
      }
    }
    $html .= '<button class="js-modal wpt-button wp-button-translations-settings" data-slug="' . esc_attr( $field['data']['textdomain'] ) . '" data-locale="' . esc_attr( $currentLocale ) . '" data-modal-prefix-class="wpt-modal" data-modal-content-id="wpt-translation-settings-' . esc_attr( $field['data']['textdomain'] ) . '-' . esc_attr( $currentLocale ) . '" data-modal-close-text="<span>' . esc_html__( 'Close modal', 'wp-translations' ) . '</span>" data-modal-close-title="' . esc_html__( 'Close modal', 'wp-translations' ) . '" title="' . esc_attr( $field['data']['textdomain'] ) . '-' . esc_attr( $currentLocale ) . '" id="label_modal_1" aria-haspopup="dialog"><span class="dashicons dashicons-admin-tools"></span>  <span class="wpt-hide-on-lg">' . esc_html__( 'Settings', 'wp-translations' ) . '</span></button>';
    $html .= ModalHelper::displayTranslationModal( $field['data']['textdomain'], $currentLocale, $args, $translationSettings );
    $html .= '</td>';

    return $html;
  }

  public static function setPageFooter() {

    $html = parent::setPageFooter();

    return apply_filters(  WPTORG_SLUG . '_translations_page_footer', $html );
  }

  public static function setPageDebug() {

    $settings = get_site_option( 'wpt_translations' );
    $debug = array(
      'translations_updates' => array(
        'label' => __( 'Translations Updates', 'wp-translations' ),
        'data'  => wp_get_translation_updates()
      ),
      'translations_settings' => array(
        'label' => __( 'Translations Settings', 'wp-translations' ),
        'data'  => $settings
      ),
    );

    return apply_filters(  WPTORG_SLUG . '_translations_page_debug', $debug );
  }

}
