<?php

namespace WP_Translations\WordPress\Admin\Page;

defined( 'ABSPATH' ) or die( 'You don&#8217;t have permission to do this.' );

use WP_Translations\WordPress\Helpers\Helper;

abstract class Page {

  public static function setPageActions() {
    $action = array();

    return $action;
  }

  public static function setTabs() {

    $tabs = array();

    return $tabs;
  }

  public static function setFields() {

    $fields = array();

    return $fields;
  }

  public static function setColumnsHeaders() {}

  public static function setRowAttribut( $tabKey, $fieldID, $field ) {

    $attributs = array(
      'row_id'    => $tabKey . '-' . $fieldID,
      'notice_id' => $tabKey . '-' . $fieldID,
    );

    return $attributs;
  }

  public static function setPageFooter() {}

  public static function setPageDebug() {}

}
