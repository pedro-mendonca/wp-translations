<?php

namespace WP_Translations\WordPress\Admin;

defined( 'ABSPATH' ) or die( 'You don&#8217;t have permission to do this.' );

use WP_Translations\Models\HooksAdminInterface;
use WP_Translations\WordPress\Helpers\Helper;
use WP_Translations\WordPress\Helpers\LoggerHelper;
use WP_Translations\WordPress\Helpers\RepositoryHelper;
use WP_Translations\WordPress\Helpers\TranslationHelper;
use WP_Translations\APIs\GlotPress_Api;
/**
 * Translations Updater
 *
 * @since 1.0.0
 */

class TranslationUpdater implements HooksAdminInterface {

  public function hooks() {
    add_filter( 'pre_set_site_transient_update_plugins',  array( $this, 'preTransientPlugins' ) );
    add_filter( 'site_transient_update_plugins',          array( $this, 'transientPlugins' ) );
  }

  public function preTransientPlugins( $transient ) {

    $options = Helper::getOptions();

    if ( ! is_object( $transient ) ) {
      $transient = new \stdClass();
    }

    require_once( ABSPATH . 'wp-admin/includes/translation-install.php' );

    $localUpdates  =  TranslationHelper::languagePacksHandler();
    $locales       = ! empty( get_available_languages() ) ? get_available_languages() : array( Helper::getLocale() );

    foreach( $localUpdates as $update ) {
      $remotePoRevision = isset( $update->updated ) ? strtotime( substr( $update->updated, 0, -3 ) ) : 0;
      $localPoRevision  = TranslationHelper::getLocalPoRevisionDate( $update->type, $update->slug, $update->language );
      if ( $remotePoRevision > $localPoRevision ) {
        $transient->translations[] = $update;
        $version = array( 'product' => $update->version, 'po_revision' => strtotime( $update->updated ) );
        LoggerHelper::log( $update->slug . '-' . $update->language, 'update-available', $_SERVER, 1, $version );
      }
    }

    return $transient;
  }

  public function transientPlugins( $value ) {

    $transient  = (array) $value;
    $updates    = array();
    $allUpdates = array();
    $translationOptions = get_site_option( 'wpt_translations' );

    if ( isset( $transient['translations'] ) && ! empty( $transient['translations'] ) ) {
      foreach ( $transient['translations'] as $key => $update ) {

        $exclude    = false;
        $update = (array) $update;


        $priority = ( isset( $translationOptions[ $update['slug'] ][ $update['language'] ]['repo_priority'] ) ) ? $translationOptions[ $update['slug'] ][ $update['language'] ]['repo_priority'] : 'all' ;

        if ( ! isset( $update['repo'] ) ) {
          $update['repo'] = 'wordpress';
        }

        if ( isset( $update['endpoint'] ) ) {
          GlotPress_Api::createPoFile( $update );
        }

        if ( 'all' != $priority && $priority != $update['repo'] ) {
          unset( $transient['translations'][ $key ] );
          $exclude = true;
        }

        if ( isset( $translationOptions[ $update['slug'] ][ $update['language'] ]['lock_translation'] ) && '1' == $translationOptions[ $update['slug'] ][ $update['language'] ]['lock_translation'] ) {
          unset( $transient['translations'][ $key ] );
          $exclude = true;
        }

        if ( false === $exclude ) {
          $allUpdates[ $update['slug'] ][ $update['language'] ] = (object) $update;
        }

      }

      foreach ( $allUpdates as $textdomain => $project ) {
        foreach( $project as $locale => $translations ) {

          if ( 1 < count( $translations ) ) {
            $update = array_filter( (array) $translations, RepositoryHelper::lastest() );
          } else {
            $update = $translations;
          }

          $updates[] = $update;
        }
      }


      $transient['translations'] = array_unique( $updates, SORT_REGULAR );
    }

    return (object) $transient;
  }

}
