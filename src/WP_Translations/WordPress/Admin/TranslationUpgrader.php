<?php

namespace WP_Translations\WordPress\Admin;

defined( 'ABSPATH' ) or die( 'You don&#8217;t have permission to do this.' );

use WP_Translations\Models\HooksAdminInterface;
use WP_Translations\WordPress\Helpers\LoggerHelper;
use WP_Translations\WordPress\Helpers\Helper;
use WP_Translations\WordPress\Helpers\FileHelper;
use WP_Translations\WordPress\Helpers\ProductHelper;
use WP_Translations\WordPress\Helpers\TranslationHelper;

/**
 * Upgrade Translations
 *
 * @since 1.0.0
 */

class TranslationUpgrader implements HooksAdminInterface {

  /**
   * @see WP_Translations\Models\HooksInterface
   */
  public function hooks() {
    add_action( 'wp_ajax_upgradeTranslation', array( $this, 'upgradeTranslation' ),-1 );
    add_action( 'upgrader_process_complete',  array( $this, 'upgradeComplete' ),10 , 2 );
    add_action( 'admin_init',                 array( $this, 'clearUpdateCache'), -1 );
  }

  public function upgradeTranslation() {

    if ( ! wp_verify_nonce( $_POST['nonce'], 'wpt-update-nonce' ) ) {
      wp_die( esc_html__( 'You don&#8217;t have permission to do this.', 'wp-translations' ) );
    }

    $status = array(
      'update' => 'translations',
    );
    if ( ! current_user_can( 'update_core' ) && ! current_user_can( 'update_plugins' ) && ! current_user_can( 'update_themes' ) ) {
      $status['errorMessage'] = __( 'You do not have sufficient permissions to update this site.', 'wp-translations' );
      wp_send_json_error( $status );
    }

    $slug       = esc_attr( $_POST['slug'] );
    $type       = esc_attr( $_POST['type'] );
    $locale     = explode( '|', $_POST['locale'] );

    include_once( ABSPATH . 'wp-admin/includes/class-wp-upgrader.php' );

    $allLanguageUpdates = wp_get_translation_updates();

    $languageUpdates = array();
    foreach ( $allLanguageUpdates as $currentLanguageUpdate ) {
      if ( $currentLanguageUpdate->slug == $slug && in_array( $currentLanguageUpdate->language, $locale ) ) {
        $languageUpdates[] = $currentLanguageUpdate;
      }
    }

    $status['lp'] = $languageUpdates;

    $skin     = new \Automatic_Upgrader_Skin();
    $upgrader = new \Language_Pack_Upgrader( $skin );
    $result   = $upgrader->bulk_upgrade( $languageUpdates, array( 'clear_update_cache' => false ) );

    $status['message'] = $upgrader->skin->get_upgrade_messages();

    if ( is_array( $result ) && is_wp_error( $skin->result ) ) {
      $result = $skin->result;
    }

    if ( ( is_array( $result ) && ! empty( $result[0] ) ) || true === $result ) {

      $status['clearCache'] = set_site_transient( 'wpt_clear_cache', '1' );
      wp_send_json_success( $status );

    } elseif ( is_wp_error( $result ) ) {

      $status['errorMessage'] = $result->get_error_message();
      wp_send_json_error( $status );

    } elseif ( false === $result ) {

      global $wp_filesystem;
      $status['errorCode']    = 'unable_to_connect_to_filesystem';
      $status['errorMessage'] = __( 'Unable to connect to the filesystem. Please confirm your credentials.', 'wp-translations' );
      // Pass through the error from WP_Filesystem if one was raised.
      if ( $wp_filesystem instanceof WP_Filesystem_Base && is_wp_error( $wp_filesystem->errors ) && $wp_filesystem->errors->get_error_code() ) {
        $status['errorMessage'] = esc_html( $wp_filesystem->errors->get_error_message() );
      }

      wp_send_json_error( $status );

    }
    // An unhandled error occurred.
    $status['errorMessage'] = __( 'Translations update failed.', 'wp-translations' );
    wp_send_json_error( $status );

  }

  public function upgradeComplete( $upgrader_object, $options ) {

    if ( 'update' == $options['action'] && 'translation' == $options['type']  ) {

      require_once( ABSPATH . 'wp-admin/includes/translation-install.php' );

      foreach( $options['translations'] as $update ) {

        $translations = wp_get_installed_translations( $update['type'] . 's' );
        $updateSlug   = TranslationHelper::rewriteTextdomain( $update['slug'] );
        $po_revision  = TranslationHelper::getLocalPoRevisionDate( $update['type'], $updateSlug, $update['language'] );
        $version      = array( 'product' => $update['version'], 'po_revision' => $po_revision );
        LoggerHelper::log( $update['slug'] . '-' . $update['language'], 'update', $_SERVER, 1, $version );

        if ( is_dir(  WPTORG_CONTENT_PATH_TEMP . '/' . $update['slug'] )  ) {
          FileHelper::removeRecursiveDir( WPTORG_CONTENT_PATH_TEMP . '/' . $update['slug'] );
        }
      }
    }
  }

  public function clearUpdateCache() {

    if ( false !== get_site_transient( 'wpt_clear_cache' ) ) {
      Helper::generateUpdateCache();
      delete_site_transient( 'wpt_clear_cache' );
    }

  }

}
