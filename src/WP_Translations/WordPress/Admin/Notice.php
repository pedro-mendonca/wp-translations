<?php

namespace WP_Translations\WordPress\Admin;

defined( 'ABSPATH' ) or die( 'You don&#8217;t have permission to do this.' );

use WP_Translations\Models\HooksAdminInterface;

/**
 * settings Actions
 *
 * @since 1.0.0
 */

class Notice implements HooksAdminInterface {

  public function hooks() {
    add_action( is_multisite() ? 'network_admin_notices' : 'admin_notices', array( $this, 'showNotices' ) );
  }

  public function showNotices() {

    $notices = array(
        'updated' => array(),
        'error'   => array(),
      );

    if ( isset( $_GET['wpt-message'] ) ) {
      $capability = is_multisite() ? 'manage_network' : 'manage_options';
      if ( current_user_can( $capability ) ) {
        switch ( $_GET['wpt-message'] ) {
          case 'settings_updated':
            $notices['updated']['wpt-settings-updated'] = __( 'Settings updated.', 'wp-translations' );
            break;

          case 'products_reloaded':
            $notices['updated']['wpt-settings-updated'] = __( 'Products list reloaded.', 'wp-translations' );
            break;

          case 'check_translations_updates':
            $notices['updated']['wpt-settings-updated'] = __( 'Check for translations updates, done!', 'wp-translations' );
            break;

          case 'reset_translations':
            $notices['updated']['wpt-settings-updated'] = __( 'Translations settings successfully reseted.', 'wp-translations' );
            break;

          case 'purge_logs':
            $notices['updated']['wpt-settings-updated'] = __( 'Logs successfully purged.', 'wp-translations' );
            break;
        }
      }
    }

    if ( count( $notices['updated'] ) > 0 ) {
      foreach ( $notices['updated'] as $notice => $message ) {
        add_settings_error( 'wpt-notices', $notice, $message, 'updated' );
      }
    }

    if ( count( $notices['error'] ) > 0 ) {
      foreach ( $notices['error'] as $notice => $message ) {
        add_settings_error( 'wpt-notices', $notice, $message, 'error' );
      }
    }

    settings_errors( 'wpt-notices' );
  }

}
