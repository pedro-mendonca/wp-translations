<?php

namespace WP_Translations\WordPress\Admin\Actions;

defined( 'ABSPATH' ) or die( 'You don&#8217;t have permission to do this.' );

use WP_Translations\Models\HooksAdminInterface;

/**
 * Locales Actions
 *
 * @since 1.0.0
 */

class LanguageActions implements HooksAdminInterface {

  public function __construct() {
    require_once( ABSPATH . 'wp-admin/includes/translation-install.php' );
    $this->availableTranslations = wp_get_available_translations();
  }

  public function hooks() {
    add_action( 'wp_ajax_addLocale',    array( $this, 'addLocale' ) );
    add_action( 'wp_ajax_deleteLocale', array( $this, 'deleteLocale' ) );
  }

  public function addLocale() {

    if ( ! wp_verify_nonce( $_POST['nonce'], 'wpt-translations-nonce' ) ) {
      wp_die( esc_html__( 'You don&#8217;t have permission to do this.', 'wp-translations' ) );
    }

    $locale = esc_attr( $_POST['locale'] );

    require_once( ABSPATH . 'wp-admin/includes/translation-install.php' );

    $installer = wp_download_language_pack( $locale );

    $html  = '<td><label for="wpt-' . $locale . '">' . $locale . '</label></td>';
    $html .= '<td>' . $this->availableTranslations[ $locale ]['native_name'] . '</td>';
    $html .= '<td>' . $this->availableTranslations[ $locale ]['english_name'] . '</td>';
    $html .= '<td class="column-actions"><button class="wpt-button danger wpt-delete-language" data-colspan="' . absint( $_POST['colspan'] ) . '" data-tab="' . esc_attr( 'locales' ) . '" data-locale="' . esc_attr( $locale ) . '"><span class="dashicons dashicons-trash"></span> ' . esc_html__( 'Delete', 'wp-translations' ) . '</button></td>';


    if ( false !== $installer ) {
      $data = array(
        'message' => $installer,
        'html'    => $html
      );
      wp_send_json_success( $data );
    } else {
      $data = array(
        'message' => esc_html__( 'Something went wrong.', 'wp-translations' )
      );
      wp_send_json_error( $data );
    }

    die();
  }

  public function deleteLocale() {

    if ( ! wp_verify_nonce( $_POST['nonce'], 'wpt-translations-nonce' ) ) {
      wp_die( esc_html__( 'You don&#8217;t have permission to do this.', 'wp-translations' ) );
    }

    $locale = esc_attr( $_POST['locale'] );
    $files  = array();
    $dirs   = apply_filters( WPTORG_SLUG . '_translations_dirs', array(
      'core'    => WP_LANG_DIR,
      'plugins' => WP_LANG_DIR . '/plugins',
      'themes'  => WP_LANG_DIR . '/themes'
    ) );

    foreach( $dirs as $type => $dir ) {
      $files[ $type ] = glob( $dir . '/*' . $locale . '.*o' );
    }

    foreach ( $files as $dir ) {
      foreach ( $dir as $file ) {
        if ( is_readable( $file ) ) {
          unlink( $file );
        }
      }
    }

    wp_send_json( $files );

    die();
  }

}
