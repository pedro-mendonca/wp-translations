<?php

namespace WP_Translations\WordPress\Admin\Actions;

defined( 'ABSPATH' ) or die( 'You don&#8217;t have permission to do this.' );

use WP_Translations\Models\HooksAdminInterface;
use WP_Translations\WordPress\Helpers\FeatureHelper;

/**
 * settings Actions
 *
 * @since 1.0.0
 */

class PerformanceActions implements HooksAdminInterface {

  public function hooks() {
    if( false !== FeatureHelper::isEnable( 'performance' ) ) {
      add_filter( WPTORG_SLUG . '_settings_fields',            array( $this, 'setFields' ) );
      add_filter( WPTORG_SLUG . '_settings_tabs',              array( $this, 'setTabs' ) );
    }
  }

  public function setFields( $fields ) {

    $fields['settings']['performance'] =  array(
      'mo_cache' => array(
        'label' => __( 'MO Cache', 'wp-translations' ),
        'type'  => 'checkbox',
        'desc'  => __( 'Cache translations with object cache', 'wp-translations' ),
        'order' => '0',
        'class' => 'teaser'
      ),
      'gettext' => array(
        'label' => __( 'Gettext', 'wp-translations' ),
        'type'  => 'select',
        'choices' => array(
          'wordpress' => __( 'WordPress gettext', 'wp-translations' ),
          'php'       => __( 'PHP native gettext', 'wp-translations' ),
        ),
        'desc'  => __( 'Select Gettext library.', 'wp-translations' ),
        'order' => '0',
        'class' => 'teaser'
      ),
    );

    return $fields;
  }

  public function setTabs( $tabs ) {

    $tabs['settings']['performance'] = array(
      'label' => __( 'Performance', 'wp-translations' ),
      'icon'  => 'dashicons-performance',
      'order' => '30',
      'class' => 'teaser'
    );

    return $tabs;
  }

}
