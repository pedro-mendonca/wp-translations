<?php

namespace WP_Translations\WordPress\Helpers;

defined( 'ABSPATH' ) or die( 'You don&#8217;t have permission to do this.' );

use WP_Translations\WordPress\Helpers\Helper;
use WP_Translations\WordPress\Helpers\RepositoryHelper;

/**
 *
 * @author Jerome SADLER
 * @since 1.0.0
 */
abstract class RoutineHelper {

  public static function triggerUpgrades() {

    $pluginVersion = get_site_option( 'wpt_version' );

    if ( ! $pluginVersion ) {
      $pluginVersion = WPTORG_VERSION;
      add_site_option( 'wpt_version', $pluginVersion );
    }

    if ( $pluginVersion && version_compare( $pluginVersion, '1.0.4', '<'  ) ) {
      $upgrade = self::updgrade104();

      if ( 'complete' == $upgrade ) {
        $pluginVersion = '1.0.4';
        update_site_option( 'wpt_version', $pluginVersion );
      }
    }

    if ( $pluginVersion && version_compare( $pluginVersion, '1.0.5', '<'  ) ) {
      $upgrade = self::purgeTranslationsSettings();

      if ( 'complete' == $upgrade ) {
        $pluginVersion = '1.0.5';
        update_site_option( 'wpt_version', $pluginVersion );
      }
    }

    if ( $pluginVersion && version_compare( $pluginVersion, '1.0.6', '<'  ) ) {
      $upgrade = self::upgrade106();

      if ( 'complete' == $upgrade ) {
        $pluginVersion = '1.0.6';
        update_site_option( 'wpt_version', $pluginVersion );
      }
    }

  }

  public static function updgrade103() {

    global $wpdb;

    $repositories = array(
      'wordpress' => array(
        'name'        => 'WordPress.ORG',
        'provider'    => 'http',
        'type'        => 'public',
        'token'       => '',
        'url'         => esc_url_raw( 'http', 'https://wordpress.org' ),
        'rescue'      => array(
          'provider' => '',
          'type'     => '',
          'token'    => '',
          'url'      => '',
        ),
        'logo'        => WPTORG_PLUGIN_URL .'assets/img/dotorg.png',
        'description' => '',
        'status'      => 'protected',
        'active'      => 1,
      ),
      'wp-translations' => array(
        'name'        => 'WP-Translations ORG',
        'provider'    => 'github',
        'type'        => 'public',
        'token'       => '',
        'url'         => esc_url_raw( 'https://github.com/WP-Translations/language-packs' ),
        'rescue'      => array(
          'provider' => 'gitlab',
          'type'     => 'public',
          'token'    => '',
          'url'      => esc_url_raw( 'https://gitlab.com/wp-translations-repo/lp2' ),
        ),
        'logo'        => WPTORG_PLUGIN_URL .'assets/img/WP-TORG.png',
        'description' => '',
        'status'      => 'protected',
        'active'      => 1,
      ),
    );
    update_site_option( 'wpt_repositories', $repositories );

    // Remove old transients
    $wpdb->query( "DELETE FROM $wpdb->options WHERE option_name LIKE '\_site\_transient\_wpt\_repository\_%'" );
    $wpdb->query( "DELETE FROM $wpdb->options WHERE option_name LIKE '\_site\_transient\_wpt\_project\_%'" );


    // CLear update Transients
    Helper::clearUpdateCache( 'all' );

    return 'complete';
  }

  public static function updgrade104() {

    $repos = get_site_option( 'wpt_repositories' );
    $repos['wordpress']['logo'] = WPTORG_PLUGIN_URL .'assets/img/dotorg.png';
    update_site_option( 'wpt_repositories', $repos );

    return 'complete';
  }

  public static function purgeTranslationsSettings() {

    delete_site_option( 'wpt_translations' );
    return 'complete';
  }

  public static function defaultSettings() {

    $defaults = array(
      'features' => array(
        'repositories' => 1,
        'premium'       => 1,
        'capabilities'  => '',
        'performance'   => '',
      ),
      'settings_ui' => array(
        'core_updates'    => 1,
        'plugins_updates' => '',
        'themes_updates'  => '',
        'bubble_count'    => 1,
        'page_position'   => 'menu',
      ),
      'updates' => array(
        'async-updates'   => 1,
      ),
      'advanced' => array(
        'debug_mode'           => '',
      ),
      'licenses'        => array(),
    );

    return $defaults;
  }

  public static function upgrade106() {

    delete_site_option( 'wpt_global_translations' );
    $purgeTranslations = self::purgeTranslationsSettings();
    $options = self::defaultSettings();
    Helper::updateOptions( $options );

    return 'complete';
  }

}
