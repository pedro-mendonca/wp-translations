<?php

namespace WP_Translations\WordPress\Helpers;

defined( 'ABSPATH' ) or die( 'You don&#8217;t have permission to do this.' );

use WP_Translations\WordPress\Helpers\Helper;
use WP_Translations\WordPress\Helpers\FeatureHelper;
use WP_Translations\WordPress\Helpers\RepositoryHelper;
use WP_Translations\WordPress\Helpers\ProductHelper;

use Gettext\Translations;
use Gettext\Generators;

/**
 *
 * @author Jerome SADLER
 * @since 1.0.0
 */
abstract class TranslationHelper {

  public static function currentLocale() {

    if ( isset( $_GET['locale'] ) ) {
      $currentLocale = esc_attr( $_GET['locale'] );
    } else {
      $currentLocale = Helper::getLocale();
    }

    if ( 'en_US' === $currentLocale ) {
      $currentLocale = '';
    }

    return $currentLocale;
  }

  public static function getPoInfos( $slug, $locale ) {

    $translationsSettings = get_site_option( 'wpt_translations' );
    $headers              = array();
    $html                 = '';

    if ( isset( $translationsSettings[ $slug ][ $locale ]['loaded_mo_file'] ) ) {
      $moFile = $translationsSettings[ $slug ][ $locale ]['loaded_mo_file'];
      if ( @is_readable( $moFile ) && ! is_dir( $moFile ) ) {
        $type   = self::getType( $moFile );

        $poPath = str_replace( '.mo', '.po', $moFile );

        if ( @is_readable( $poPath ) && ! is_dir( $poPath ) ) {
          $headers = wp_get_pomo_file_data( $poPath );
        } else {
          $customPo = WPTORG_CONTENT_DIR . '/embeded/' . $slug . '-' . $locale .'.po';
          if ( ! @is_readable( $customPo ) ) {
            $translations = Translations::fromMoFile( $moFile );
            $translations->toPoFile( $customPo );
          }
          $poFile  = Translations::fromPoFile( $customPo );
          $headers = wp_get_pomo_file_data( $customPo );
        }
      }
    }
    if ( ! empty( $headers ) ) {
      $html  = '<h4>' . $slug . '-' . $locale . '.po</h4>';
      $html .= '<ul>';
      foreach( $headers as $key => $header ) {
          $html .= '<li>' . $key . ': <strong>' . $header .'</strong></li>';
      }
      $html .= '</ul>';
    }
    return $html;
  }

  public static function getLocalPoRevisionDate( $type, $slug, $locale ) {

    $translations         = ( 'core' == $type ) ? wp_get_installed_translations( 'core' ) : wp_get_installed_translations( $type.'s' );
    $translationsSettings = get_site_option( 'wpt_translations' );

    if ( isset( $translations[ $slug ][ $locale ] ) ) {

      switch ( $slug ) {
        case 'Divi':
          $textdomains = array(
            'Divi'       => strtotime( $translations['Divi'][ $locale ]['PO-Revision-Date'] ),
            'et_builder' => strtotime( $translations['et_builder'][ $locale ]['PO-Revision-Date'] ),
            'et-core'    => strtotime( $translations['et-core'][ $locale ]['PO-Revision-Date'] )
          );
          $poRevision = max( $textdomains );
          break;

        case 'bloom':
          $textdomains = array(
            'bloom'        => strtotime( $translations['bloom'][ $locale ]['PO-Revision-Date'] ),
            'et_dashboard' => strtotime( $translations['et_dashboard'][ $locale ]['PO-Revision-Date'] ),
          );
          $poRevision = max( $textdomains );
          break;

        case 'extra':
          $textdomains = array(
            'extra'       => strtotime( $translations['extra'][ $locale ]['PO-Revision-Date'] ),
            'Extra'       => strtotime( $translations['Extra'][ $locale ]['PO-Revision-Date'] ),
            'et_builder'  => strtotime( $translations['et_builder'][ $locale ]['PO-Revision-Date'] ),
            'et-core'     => strtotime( $translations['et-core'][ $locale ]['PO-Revision-Date'] )
          );
          $poRevision = max( $textdomains );
          break;

        default:
          $poRevision = strtotime( substr( $translations[ $slug ][ $locale ]['PO-Revision-Date'], 0, -3 ) );
          break;
      }
    } else {

      if ( isset( $translationsSettings[ $slug ][ $locale ]['loaded_mo_file'] ) ) {
        $moFile = $translationsSettings[ $slug ][ $locale ]['loaded_mo_file'];
        $type   = self::getType( $moFile );

        if( 'plugin-dir' == $type || 'theme-dir' == $type ) {

          $poPath = str_replace( '.mo', '.po', $moFile );

          if ( @is_readable( $poPath ) && ! is_dir( $poPath ) ) {
            $headers = wp_get_pomo_file_data( $poPath );
            $poRevision = strtotime( $headers['PO-Revision-Date'] );
          } else {
            $customPo = WPTORG_CONTENT_DIR . '/embeded/' . $slug . '-' . $locale .'.po';
            if ( ! @is_readable( $customPo ) ) {
              $translations = Translations::fromMoFile( $moFile );
              $translations->toPoFile( $customPo );
            }
            $poFile     = Translations::fromPoFile( $customPo );
            $poRevision = strtotime( $poFile->getHeader( 'PO-Revision-Date' ) );
          }
        } else {
          $poRevision = 0;
        }
      } else {
        $poRevision = 0;
      }
    }

    return $poRevision;
  }

  /**
   * Get the $type property based on a best guess of the type of WP add-on this text-domain file is used for.
   *
   * @param string $file Path to an mo-file.
   *
   * @return string Add-on type.
   */
  public static function getType( $file ) {

    if ( false !== strpos( $file, WP_LANG_DIR ) ) {
      return 'wp-lang-dir';
    } elseif ( false !== strpos( $file, WP_PLUGIN_DIR ) ) {
      return 'plugin-dir';
    } elseif (  false !== strpos( $file, WP_CONTENT_DIR .'/themes/' ) ) {
      return 'theme-dir';
    } else {
      return 'undefined';
    }
  }

  public static function rewriteTextdomain( $slug, $action = 'lowercase' ) {

    switch ( $action ) {
      case 'lowercase':
        $domainsToRewrite = array(
          'extra' => 'Extra',
          'divi'	=> 'Divi',
        );
        break;

      case 'capitalize':
        $domainsToRewrite = array(
          'Extra' => 'extra',
          'Divi'	=> 'divi',
        );
        break;
    }

    $rewrite = ( in_array( $slug, array_keys( $domainsToRewrite ) ) ) ? $domainsToRewrite[ $slug ] : $slug;

    return $rewrite;
  }

  public static function sanitizeTextdomain( $slug ) {
    return sanitize_title( $slug );
  }

  /**
   * Get all translations
   *
   * @param $args array
   *
   * @return array
   */
  public static function getAllTranslations( $args = array() ) {

    $plugins = get_plugins();
    $data    = array();
    $i       = 0;

    foreach ( $plugins as $file => $plugin ) {
      $updates = wp_get_translation_updates( 'plugins' );

      $plugins_updates = array();
      foreach ( $updates as $update ) {
        if ( $plugin['TextDomain'] === $update->slug ) {
          $plugins_updates[] = $update;
        }
      }

      $installed = wp_get_installed_translations( 'plugins' );
      $plugins_installed = ( isset($installed[ $plugin['TextDomain'] ]) ) ? $installed[ $plugin['TextDomain'] ] : array();

      $data[] = (object) array(
        'id'         => $i++,
        'name'       => $plugin['Name'],
        'textdomain' => $plugin['TextDomain'],
        'version'    => $plugin['Version'],
        'type'       => 'plugin',
        'file'       => $file,
        'updates'    => $plugins_updates,
        'installed'  => $plugins_installed,
      );
    }

    $themes = array_keys( wp_get_themes() );
    foreach ( $themes as $theme ) {

      $theme_data     = wp_get_theme( $theme );
      $updates        = wp_get_translation_updates( 'themes' );
      $domain         = empty( $theme_data->get( 'TextDomain' ) ) ? $theme_data->get( 'Name' ) : $theme_data->get( 'TextDomain' );
      $textdomain     = apply_filters( 'wpt_rewrite_textdomains', $domain );
      $themes_updates = array();

      foreach ( $updates as $update ) {
        if ( $textdomain === $update->slug ) {
          $themes_updates[] = $update;
        }
      }

      $installed = wp_get_installed_translations( 'themes' );
      $themes_installed = ( isset( $installed[ $domain ] ) ) ? $installed[ $domain ] : array();

      $data[] = (object) array(
        'id'         => $i++,
        'name'       => $theme_data->get( 'Name' ),
        'textdomain' => $textdomain,
        'version'    => $theme_data->get( 'Version' ),
        'type'       => 'theme',
        'updates'    => $themes_updates,
        'installed'  => $themes_installed,
      );
    }

    $core = wp_get_installed_translations('core');
    if( ! empty( $core ) ) {
      foreach ( $core as $key => $translation ) {
        $updates      = wp_get_translation_updates( 'core' );
        $core_updates = array();
        foreach ( $updates as $update ) {
          if ( $key === $update->slug ) {
            $core_updates[] = $update;
          }
        }

        $core_installed = ( isset( $core[ $key ] ) ) ? $core[ $key ]: array();
        global $wp_version;

        $data[] = (object) array(
          'id'         => $i++,
          'name'       => ( '' == self::currentLocale() ) ? ucfirst( $key) : $translation[ self::currentLocale() ]['Project-Id-Version'],
          'textdomain' => $key,
          'version'    => $wp_version,
          'type'       => 'core',
          'updates'    => $core_updates,
          'installed'  => $core_installed,
        );
      }
    }

    return $data;
  }

  public static function languagePacksHandler() {

    $options       = Helper::getOptions();
    $languagePacks = array();

    if ( false !== FeatureHelper::isEnable( 'repositories' ) ) {
      $repoLanguagePacks = RepositoryHelper::getProjectsUpdates();
      $languagePacks = array_merge( $languagePacks, $repoLanguagePacks );
    }
    if ( false !== FeatureHelper::isEnable( 'premium' ) ) {
      $proLanguagePacks  = ProductHelper::getProductsUpdates();
      $languagePacks = array_merge( $languagePacks, $proLanguagePacks );
    }

    return $languagePacks;
  }

  public static function allType() {

    $type = array(
      'plugin' => array(
          'label' => __( 'Plugins', 'wp-translations' ),
          'icon'  => 'dashicons-admin-plugins'
      ),
      'theme'  => array(
        'label' => __( 'Themes', 'wp-translations' ),
        'icon'  => 'dashicons-admin-appearance'
      ),
      'core'   => array(
        'label' => __( 'Core', 'wp-translations' ),
        'icon'  => 'dashicons-wordpress'
      )
    );

    return $type;
  }

  public static function getSettingsFields() {

    $settings = array(
      'repo-priority' => array(
        'type'     => 'select',
        'required' => false,
        'label'    => __( 'Allow updates from', 'wp-translations' ),
        'order'    => '0',
        'hidden'   => 0,
        'choices'  => RepositoryHelper::dropdownRepos()
      ),
      'lock-translation' => array(
        'type'     => 'checkbox',
        'required' => false,
        'label'    => __( 'Lock translation', 'wp-translations' ),
        'order'    => '20',
        'hidden'   => 0
      ),
      'use_custom' => array(
        'type'     => 'checkbox',
        'required' => false,
        'label'    => __( 'Custom translation', 'wp-translations' ),
        'order'    => '40',
        'hidden'   => 0
      ),
      'custom_translation' => array(
        'type'     => 'file',
        'required' => true,
        'label'    => __( 'Upload custom file (mo/po)', 'wp-translations' ),
        'order'    => '41',
        'hidden'   => 1,
        'condition' => array(
          'use_custom' => array( '1' )
        ),
      )
    );

    return apply_filters( WPTORG_SLUG . '_translations_settings_fields', $settings );
  }

}
