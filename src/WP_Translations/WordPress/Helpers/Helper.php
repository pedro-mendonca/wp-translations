<?php

namespace WP_Translations\WordPress\Helpers;

defined( 'ABSPATH' ) or die( 'You don&#8217;t have permission to do this.' );

use WP_Translations\WordPress\Helpers\RepositoryHelper;

/**
 *
 * @author Jerome SADLER
 * @since 1.0.0
 */
abstract class Helper {

    public static function adminScreen() {

      $screens = array(

        'toplevel_page_' . WPTORG_SLUG,
        'toplevel_page_' . WPTORG_SLUG . '-network',
        'settings_page_' . WPTORG_SLUG,
      );

      return $screens;
    }

    public static function assetsScreen() {

      $admin  = self::adminScreen();
      $assets = array(
        'plugins',
        'themes',
      );

      $screens = array_merge( $admin, $assets );

      return $screens;
    }

    public static function adminUrl( $args = false ) {
      $url = is_multisite() ? network_admin_url( $args ) : admin_url( $args );

      return esc_url( $url );
    }

    public static function getOptions() {

      $defaults = array(
        'features' => array(
          'repositories' => 1,
          'premium'       => 1,
          'capabilities'  => '',
          'performance'   => '',
        ),
        'settings_ui' => array(
          'core_updates'    => 1,
          'plugins_updates' => '',
          'themes_updates'  => '',
          'bubble_count'    => 1,
          'page_position'   => 'menu',
        ),
        'updates' => array(
          'async-updates'   => 1,
        ),
        'languages' => array(
          'front_user_locale' => '',
        ),
        'advanced' => array(
          'debug_mode'           => '',
        ),
        'licenses'        => array(),
      );

      $options = ( false === get_site_option( 'wpt_settings' ) ) ? $defaults : get_site_option( 'wpt_settings' );
      return $options;
    }

    public static function updateOptions( $options ) {
      update_site_option( 'wpt_settings', $options );
    }

    public static function getIcon() {

      $user_id = get_current_user_id();
      $current_color = get_user_option( 'admin_color', $user_id );
      if (
        'midnight' == $current_color ||
        'fresh' 	 == $current_color ||
        'light'    == $current_color
      ) {
        $icon = 'wpt-icon';
      } else {
        $icon = 'wpt-icon-white';
      }

      return $icon;
    }

    public static function displayArrayRecursively( $arr ) {
      if ( $arr ) {
          foreach ( $arr as $key => $value ) {

              if ( is_array( $value ) || is_object( $value ) ) {
                $type = ( is_object( $value ) ) ? 'object' : 'array';
                echo '<h4 class="js-expandmore">' . $key . ' (' . $type . ')</h4>
                <div class="js-to_expand">
                <ul>';
                  self::displayArrayRecursively( $value );
                echo '</ul></div>';
              } else {
                  //  Output
                  echo '<li class="'. $key .'"><div><span class="wpt-debug-key">' . $key . '</span>: <span class="wpt-debug-value">' . $value . '</span></div></li>';
              }

          }
      }
  }

  public static function clearUpdateCache( $type = 'all' ) {

    $repos = RepositoryHelper::getAllRepos();
    foreach ( $repos as $key => $repo ) {
      delete_site_transient( 'wpt_repository_' . $key );
    }

    switch ( $type ) {
      case 'plugins':
        wp_clean_plugins_cache();
        break;
      case 'themes':
        wp_clean_themes_cache();
        break;
      case 'all':
        wp_clean_update_cache();
        break;
    }

  }

  public static function generateUpdateCache( $type = 'all' ) {

    self::clearUpdateCache( $type );

    switch ( $type ) {
      case 'plugins':
        wp_update_plugins();
        break;
      case 'themes':
        wp_update_themes();
        break;
      case 'all':
        wp_update_plugins();
        wp_update_themes();
        break;
    }

  }

  /**
   * Get current Locale
   *
   * @return string Locale
   */
  public static function getLocale() {
    global $wp_version;

    $locale = ( version_compare( $wp_version, '4.7', '>=' ) ) ? get_user_locale() : get_locale();

    return $locale;
  }


  /**
   * Print out option html elements for role selectors.
   *
   * @since 1.0.3
   *
   * @param string $selected Slug for the role that should be already selected.
   */
  public static function dropdownRoles( $selected = '' ) {
    $r = '';

    $editable_roles = array_reverse( get_editable_roles() );

    foreach ( $editable_roles as $role => $details ) {
      $name = translate_user_role($details['name'] );
      // preselect specified role
      if ( $selected == $role ) {
        $r .= "\n\t<option selected='selected' value='" . esc_attr( $role ) . "'>$name</option>";
      } else {
        $r .= "\n\t<option value='" . esc_attr( $role ) . "'>$name</option>";
      }
    }

    return $r;
  }

  public static function dropdownCaps( $selected = '' ) {
    $r = '';

    $caps = array(
      'install_languages' => esc_html__( 'Install languages', 'wp-translations' ),
      'update_languages' => esc_html__( 'Update translations', 'wp-translations' ),
    );

    foreach ( $caps as $key => $cap ) {
      if ( $selected == $key ) {
        $r .= "\n\t<option selected='selected' value='" . esc_attr( $key ) . "'>$cap</option>";
      } else {
        $r .= "\n\t<option value='" . esc_attr( $key ) . "'>$cap</option>";
      }
    }

    return $r;
  }

  public static function remove_filters_for_anonymous_class( $hook_name = '', $class_name = '', $method_name = '', $priority = 0 ) {
    global $wp_filter;
    // Take only filters on right hook name and priority
    if ( ! isset( $wp_filter[ $hook_name ][ $priority ] ) || ! is_array( $wp_filter[ $hook_name ][ $priority ] ) ) {
      return false;
    }
    // Loop on filters registered
    foreach ( (array) $wp_filter[ $hook_name ][ $priority ] as $unique_id => $filter_array ) {
      // Test if filter is an array ! (always for class/method)
      if ( isset( $filter_array['function'] ) && is_array( $filter_array['function'] ) ) {
        // Test if object is a class, class and method is equal to param !
        if ( is_object( $filter_array['function'][0] ) && get_class( $filter_array['function'][0] ) && get_class( $filter_array['function'][0] ) == $class_name && $filter_array['function'][1] == $method_name ) {
          // Test for WordPress >= 4.7 WP_Hook class (https://make.wordpress.org/core/2016/09/08/wp_hook-next-generation-actions-and-filters/)
          if ( is_a( $wp_filter[ $hook_name ], 'WP_Hook' ) ) {
            unset( $wp_filter[ $hook_name ]->callbacks[ $priority ][ $unique_id ] );
          } else {
            unset( $wp_filter[ $hook_name ][ $priority ][ $unique_id ] );
          }
        }
      }
    }
    return false;
  }

}
