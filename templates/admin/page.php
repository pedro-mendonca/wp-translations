<?php
/**
 * Page Settings
 *
 * @package     WP_Translations
 * @subpackage  templates/admin
 * @since      1.0.0
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

use WP_Translations\WordPress\Helpers\Helper;
use WP_Translations\WordPress\Helpers\PageHelper;
use WP_Translations\WordPress\Helpers\TableHelper;

$currentPage   = isset( $_GET['wpt-page'] ) ? $_GET['wpt-page'] : PageHelper::getPrimaryPage();
$page          = PageHelper::getPage( $currentPage );
$options       = Helper::getOptions();

$tabs          = call_user_func( array( 'WP_Translations\WordPress\Admin\Page\Page' . ucfirst( $currentPage ), 'setTabs' ) );
$fields        = call_user_func( array( 'WP_Translations\WordPress\Admin\Page\Page' . ucfirst( $currentPage ), 'setFields' ) );
$tableActions  = call_user_func( array( 'WP_Translations\WordPress\Admin\Page\Page' . ucfirst( $currentPage ), 'setPageActions' ) );
$tablesHeaders = call_user_func( array( 'WP_Translations\WordPress\Admin\Page\Page' . ucfirst( $currentPage ), 'setColumnsHeaders' ) ) ;

?>
<div class="wpt-box postbox">
  <form id="wpt-<?php echo esc_attr( $currentPage ); ?>-form" action="" method="POST">
    <h2>
      <span><span class="dashicons <?php echo esc_attr( $page['icon'] ); ?>"></span> <?php echo esc_html( $page['label'] ); ?></span>
      <div class="wpt-table-actions">
        <?php
          if ( ! empty( $tableActions ) ) {
            foreach( $tableActions as $action ) {
              echo $action;
            }
          }
        ?>
      </div>
    </h2>

    <div class="js-tabs wpt-settings-tabs">

     <ul class="js-tablist" data-tabs-prefix-class="wpt-settings">

        <?php foreach ( $tabs[ $currentPage ] as $tabKey => $tab ) :
          $isSelected = ( isset( $tab['selected'] ) ) ? $tab['selected'] : '';
          $tabClasses = ( isset( $tab['class'] ) ) ? esc_attr( $tab['class'] ) : '';
          if ( 1 < count( $tabs[ $currentPage ] ) ) : ?>
          <li class="js-tablist__item <?php echo esc_attr( $tabClasses );?>">
            <a href="#<?php echo esc_attr( $tabKey ); ?>" id="label_<?php echo esc_attr( $tabKey ); ?>" class="js-tablist__link" <?php echo $isSelected; ?>><span class="dashicons <?php echo esc_attr( $tab['icon'] ); ?>"></span> <span class="wpt-hide-on-lg"><?php echo esc_html( $tab['label'] ); ?></span></a>
          </li>
        <?php
            endif;
          endforeach; ?>

      </ul><!-- /end .js-tablist -->

      <?php foreach ( $tabs[ $currentPage ] as $tabKey => $tab ) :
        $hasMulitpleTabs = ( 1 < count( $tabs[ $currentPage ] ) ) ? 'js-tabcontent' : 'wpt-tab-alone';
      ?>
      <div id="<?php echo esc_attr( $tabKey ); ?>" class="<?php echo esc_attr( $hasMulitpleTabs ); ?>">
        <table id="wpt-<?php echo esc_attr( $currentPage ); ?>-<?php echo esc_attr( $tabKey ); ?>-table" class="wpt-settings-table row-border">

          <thead>
            <tr>
              <?php $columns = $tablesHeaders[ $currentPage ][ $tabKey ];
              foreach( $columns as $column ) : ?>
              <th scope="col" class="<?php echo esc_attr( $column['class'] ); ?>"><?php echo esc_html( $column['label'] ); ?></th>
              <?php endforeach; ?>
            </tr>
          </thead>

          <tbody id="<?php echo esc_attr( $tabKey ); ?>-rows">

            <?php if ( ! empty( $fields[ $currentPage ][ $tabKey ] ) ) :
              foreach( $fields[ $currentPage ][ $tabKey ] as $fieldID => $field ) :
                $rowAttr = call_user_func_array( array( 'WP_Translations\WordPress\Admin\Page\Page' . ucfirst( $currentPage ), 'setRowAttribut' ), array( $tabKey, $fieldID, $field )  );
                $rowClasses = ( isset( $field['class'] ) ) ? esc_attr( $field['class'] ) : '';
              ?>

            <tr id="wpt-table-row-<?php echo esc_attr( $rowAttr['row_id'] ); ?>" class="wpt-license-row <?php echo $rowClasses; ?>">
              <?php foreach( $tablesHeaders[ $currentPage ][ $tabKey ] as $columnID => $column ) {
                $td = call_user_func_array( array( 'WP_Translations\WordPress\Admin\Page\Page' . ucfirst( $currentPage ), 'getColumn_' . $columnID ), array( $tabKey, $columnID, $fieldID, $field ) );
                echo $td;
              } ?>
            </tr>
            <?php if( $currentPage != 'logs' ) : ?>
            <tr id="wpt-row-notice-<?php echo esc_attr( $rowAttr['notice_id'] ); ?>" class="wpt-license-notice hidden">
              <?php $colspan = call_user_func_array( array( 'WP_Translations\WordPress\Admin\Page\Page' . ucfirst( $currentPage ), 'getColumnsCount' ), array( $tabKey ) ); ?>
              <td colspan="<?php echo absint( $colspan ); ?>"></td>
            </tr>
          <?php endif; ?>
          <?php endforeach;
            endif;
          ?>

          </tbody>
        </table>
      </div><!-- /end js-tabcontent -->

    <?php endforeach; ?>

    </div><!-- /end .wpt-licenses-tabs -->

      <?php
      $pageFooter = call_user_func( array( 'WP_Translations\WordPress\Admin\Page\Page' . ucfirst( $currentPage ), 'setPageFooter' ) );
      if ( ! empty( $pageFooter ) ) : ?>
      <div class="wpt-table-footer">
        <?php echo $pageFooter; ?>
      </div>
      <?php endif; ?>
  </form>
</div><!-- /end .wpt-box -->


<?php if ( false !== (bool) $options['advanced']['debug_mode'] ) : ?>
<div class="postbox">
  <h2>Debug</h2>
  <div class="inside">
    <?php
    $pageDebug = call_user_func( array( 'WP_Translations\WordPress\Admin\Page\Page' . ucfirst( $currentPage ), 'setPageDebug' ) );
    if ( ! empty( $pageDebug ) ) {
      foreach ( $pageDebug as $key => $debug ) {
        echo '<div class="wpt-debug">';
          echo '<h3>' . esc_html( $debug['label'] ) . '</h3>';
          Helper::displayArrayRecursively( $debug['data'] );
        echo '</div>';
      }
    }
    ?>
  </div>
</div>
<?php endif; ?>
